import numpy as np


class Rescal2Tensor(object):
    '''
    Object that holds the matrices of the RESCAL2 Decomposition and allows
    operations on it like indexing
    '''

    def __init__(self, A, R, dtype=np.float32):
        '''
        Latent Factor Matrices
        A : 2D ndarray, Latent Factor Matrix for Entities
        R : list of 2D ndarray, Latent Factor Matrices for Relation Types
        '''
        self.A = A
        self.R = R
        self.dtype = dtype

    def __getitem__(self, index):
        if isinstance(index, tuple)and len(index) == 3:
            n = 0
            m = 0
            k = 0
            if not isinstance(index[2], int):
                raise NotImplementedError

            # handle first index
            if isinstance(index[0], slice):
                if index[0].start is None and index[0].stop is None:
                    n = None
                else:
                    raise NotImplementedError
            elif index[0] is None:  # type_constraints are None
                n = None
            elif isinstance(index[0], int):
                n = [index[0]]
            elif (isinstance(index[0], (np.ndarray, np.memmap)) and
                  index[0].ndim == 1):
                n = index[0]
            else:
                raise NotImplementedError

            # handle second index
            if isinstance(index[1], slice):
                if index[1].start is None and index[1].stop is None:
                    m = None
                else:
                    raise NotImplementedError
            elif index[1] is None:  # type_constraints are None
                m = None
            elif isinstance(index[1], int):
                m = [index[1]]
            elif (isinstance(index[1], (np.ndarray, np.memmap)) and
                  index[0].ndim == 1):
                m = index[1]
            else:
                raise NotImplementedError

            # handle third index
            if isinstance(index[2], slice):
                raise NotImplementedError

            # calculate the scores
            if n is None and m is None:
                values = self.A.dot(self.R[index[2]]).dot(self.A.T)
            elif n is None:
                values = self.A.dot(self.R[index[2]]).dot(self.A[m].T)
            elif m is None:
                values = self.A[n].dot(self.R[index[2]]).dot(self.A.T)
            else:
                values = self.A[n].dot(self.R[index[2]]).dot(self.A[m].T)

            return values

        # just an integer selects a frontal slice
        elif type(index).__name__ == 'int':  # Return a complete frontal slice
            return self[:, :, index]

        # and array of indices
        elif isinstance(index, (np.memmap, np.ndarray)) and index.ndim == 2:
            values = np.zeros((index.shape[0],), dtype=self.dtype)
            sidx = np.argsort(index, axis=0)[:, 2]  # sort order of slices
            index = index[sidx]
            current = index[0, 2]
            z = 0
            for i in xrange(index.shape[0]):
                if current != index[i, 2]:
                    values[z:i] = self.__indexing_helper(index[z:i])
                    z = i
                    current = index[i, 2]
            else:
                values[z:] = self.__indexing_helper(index[z:])

            return values[np.argsort(sidx)]
        else:
            raise NotImplementedError

    def __indexing_helper(self, indices):
        """
        Helper function for doing row wise multiplication of ARAt
        indices : 2D ndarray, indices
        """
        sidx = np.argsort(indices, axis=0)[:, 0]  # sort by rows
        subindices = indices[sidx]
        values = np.zeros((subindices.shape[0],), dtype=self.dtype)
        current = subindices[0, 0]
        z = 0
        for i in xrange(subindices.shape[0]):
            if current != subindices[i, 0]:
                values[z:i] = (self.A[current].dot(self.R[subindices[0, 2]])
                               .dot(self.A[subindices[z:i, 1]].T))
                z = i
                current = subindices[i, 0]
        else:
            values[z:] = (self.A[current].dot(self.R[subindices[0, 2]])
                          .dot(self.A[subindices[z:, 1]].T))

        return values[np.argsort(sidx)]
