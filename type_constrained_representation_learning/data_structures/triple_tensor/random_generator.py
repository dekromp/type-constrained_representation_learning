import numpy as np
from scipy.sparse import lil_matrix
from ..triple_tensor import LilTripleTensor
from ..triple_tensor import CooTripleTensor


def random_splist(n, m, k, rng):
    """Generate random sparse tensor.
    """
    tensor = []
    for i in xrange(k):
        tensor.append(lil_matrix(np.around(rng.rand(n, m) * 0.6)))
    return tensor


def random_cp_generated_splist(n, m, k, rng):
    """Generate an approximately low rank random sparse tensor.
    """
    tensor = []
    r = n/2
    A = np.array(rng.normal(0, 1e-2, (n, r)), dtype=np.float32)
    B = np.array(rng.normal(0, 1e-2, (n, r)), dtype=np.float32)
    C = np.array(rng.normal(0, 1e-2, (n, r)), dtype=np.float32)

    for z in xrange(k):
        fslice = np.empty((n, m), dtype=np.float32)
        for i in xrange(n):
            for j in xrange(m):
                fslice[i, j] = np.sum(A[i]*B[j]*C[z])
        fslice = lil_matrix(np.abs(np.around(fslice/np.max(fslice)*0.8)))
        tensor.append(fslice)

    return tensor


def random_splist_without_tc(n, m, k, seed, func=random_splist,
                             constructor='CooTripleTensor'):
    rng = np.random.RandomState(seed)
    tensor = func(n, m, k, rng)
    if constructor == "CooTripleTensor":
        tensor = [ti.tocoo() for ti in tensor]
    return eval(constructor)(tensor)


def random_splist_with_tc_mix(n, m, k, seed, func=random_splist,
                              constructor="CooTripleTensor"):
    tensor = random_splist_with_approx_tc(n, m, k, seed,
                                          constructor=constructor)
    for i in xrange(k):
        tensor.type_constraints[i][1] = None
    return tensor


def random_splist_with_approx_tc(n, m, k, seed, func=random_splist,
                                 constructor="CooTripleTensor"):
    rng = np.random.RandomState(seed)
    tensor = random_splist(n, m, k, rng)
    if constructor == "CooTripleTensor":
        tensor = [ti.tocoo() for ti in tensor]
    return eval(constructor)(tensor, approximate_type_constraints=True)
