from lil_triple_tensor import LilTripleTensor
from coo_triple_tensor import CooTripleTensor
from random_generator import *
from sampling import *
from splitting import *
from validate import *
