import numpy as np
import logging
import triple_tensor
import coo_triple_tensor
_log = logging.getLogger(__name__)


class LilTripleTensor(triple_tensor.TripleTensor):
    """
    3D Sparse Tensor Datastructure that represents the tensor as a list of
    scipy.sparse.lil_matrix as frontal slices. Used for smaller tensors with
    faster Indexing.

    Parameters
    ----------
    sparse_list : list of scipy.sparse matrices or a nx3 ndarray of indices
        (subject,object,predicate)).
    type_constraints : list of tuples or lists of two int vectors
        Each vector holds the indices of the domain and range constraints
        e.g.: [ [ndarray([0,1,2,3]),ndarray([4,5,6])] , [...,..] ]
    approximate_type_constraints : bool
        If True, type constraints are approximated based on the observed
        subject/object entities in each relation type. (Local closed-world
        assumption)
    dtype : numpy dtype
        float dtype of the data in the tensor.

    """
    def __init__(self, sparse_list, type_constraints=None,
                 approximate_type_constraints=False, dtype=np.float32):
        if type(sparse_list).__name__ == 'tuple':
            triples = sparse_list[0]
            shape = sparse_list[1]
            sparse_list = self._initialize_from_triples(triples, shape)
            sparse_list = [ti.tolil() for ti in sparse_list]

        for i in xrange(len(sparse_list)):
            if(sparse_list[i].shape[0] != sparse_list[0].shape[0] or
               sparse_list[i].shape[1] != sparse_list[0].shape[1]):
                raise(Exception('All matrices in the list must have the same'
                                'shape'))
            if type(sparse_list[0]).__name__ != 'lil_matrix':
                raise(Exception('All matrices have to be in '
                                'scipy.sparse.lil_matrix format'))
            sparse_list[i] = sparse_list[i].astype(dtype)

        if approximate_type_constraints:  # infer type_constraints
            type_constraints = [[np.unique(ti.tocoo().row),
                                 np.unique(ti.tocoo().col)]
                                for ti in sparse_list]

        if type_constraints is None:
            type_constraints = [[None, None] for ti in sparse_list]

        self.internal_type = 'lil_matrix'
        self._sparse_list = sparse_list
        self._nnz = None
        self.type_constraints = type_constraints
        self.dtype = dtype
        self._row = None
        self._col = None
        self._fslice = None
        self._data = None
        self._size = None
        self._shape = None
        self._sparsity = None

    def copy(self):
        return (LilTripleTensor([ti.copy() for ti in self._sparse_list],
                                self.type_constraints, dtype=self.dtype))

    def tocoo(self):
        return coo_triple_tensor.CooTripleTensor(
                    [ti.tocoo() for ti in self._sparse_list],
                    self.type_constraints, dtype=self.dtype)

    def approximate_type_constraints(self):
        for t in xrange(self.shape[2]):
            ti = self[t].tocoo()
            self.type_constraints[t] = [np.unique(ti.row), np.unique(ti.col)]

    def __getitem__(self, index):
        """
        Defines the indexing on the object instance
        """
        if isinstance(index, (int, np.int32, slice)):  # e.g.[1] or [1:5]
            return self._sparse_list[index]
        elif isinstance(index, tuple):  # e.g.[1,2,4] or [1:3,3:5,8]
            return self._sparse_list[index[2]][index[0], index[1]]
        elif (isinstance(index, (np.ndarray, np.memmap)) and
              index.ndim == 2 and index.shape[1] == 3 and
              isinstance(index[0, 0], (int, np.int32))):
            return self.__get_multiple_items(index)
        elif isinstance(index, (np.ndarray, np.memmap)) and index.ndim == 1:
            return self[tuple(index)]
        else:
            raise NotImplementedError

    def __setitem__(self, index, x):
        """
        Assign Values to triples and add/delete triples
        """
        if isinstance(index, (int, np.int32, slice)):  # e.g.[1] or [1:5]
            if isinstance(x, list):
                x = [xi.tolil() for xi in x]
            else:
                x = x.tolil()
            self._sparse_list[index] = x
        elif isinstance(index, tuple):  # e.g.[1,2,4] or [1:3,3:5,8]
            self._sparse_list[index[2]][index[0], index[1]] = x
        elif (isinstance(index, (np.ndarray, np.memmap)) and
              index.ndim == 2 and index.shape[1] == 3 and
              isinstance(index[0, 0], (int, np.int32))):
            self.__set_multiple_items(index, x)
        elif isinstance(index, (np.ndarray, np.memmap)) and index.ndim == 1:
            self[tuple(index)] = x
        else:
            raise NotImplementedError

    def __get_multiple_items(self, indices):
        """
        Get all values from indices
        """
        # get the values
        values = np.zeros((indices.shape[0],), dtype=np.float32)
        sidx = np.argsort(indices, axis=0)[:, 2]  # sort order of slices
        indices = indices[sidx]
        current = indices[0, 2]
        z = 0
        for i in xrange(indices.shape[0]):
            if current != indices[i, 2]:
                values[z:i] = (
                    self._sparse_list[current][indices[z:i, 0],
                                               indices[z:i, 1]]
                    .todense().flatten())
                z = i
                current = indices[i, 2]
        else:
            values[z:] = (
                self._sparse_list[current][indices[z:, 0],
                                           indices[z:, 1]].todense().flatten())
        indices = indices[np.argsort(sidx)]
        return values[np.argsort(sidx)]

    def __set_multiple_items(self, indices, x):
        """
        Setting multiple indices
        indices : nx3 ndarray of indices
        x : scalar or vector of values
        """
        sidx = np.argsort(indices, axis=0)[:, 2]  # sort order of slices
        indices = indices[sidx]
        current = indices[0, 2]
        z = 0
        if type(x).__name__ == 'int':
            for i in xrange(indices.shape[0]):
                if current != indices[i, 2]:
                    self._sparse_list[current][indices[z:i, 0],
                                               indices[z:i, 1]] = x
                    z = i
                    current = indices[i, 2]
            else:
                self._sparse_list[current][indices[z:, 0], indices[z:, 1]] = x
        else:
            x = x[sidx]
            for i in xrange(indices.shape[0]):
                if current != indices[i, 2]:
                    self._sparse_list[current][indices[z:i, 0],
                                               indices[z:i, 1]] = x[z:i]
                    z = i
                    current = indices[i, 2]
            else:
                self._sparse_list[current][indices[z:, 0],
                                           indices[z:, 1]] = x[z:]
            x = x[np.argsort(sidx)]

        indices = indices[np.argsort(sidx)]
