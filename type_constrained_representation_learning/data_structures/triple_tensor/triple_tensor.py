import numpy as np
import abc
import logging
from scipy.sparse import coo_matrix
from .sampling import unique_rows
_log = logging.getLogger(__name__)


class TripleTensor(object):
    """
    3D Sparse Tensor Datastructure that represents the tensor as a list of
    sparse matrices as frontal slices.

    Parameters
    ----------
    sparse_list : list of :class:`scipy.sparse coo_matrices` or
        :class:`numpy.ndarray`
        In case of the numpy array, it is expected that it is of shapt nx3
        (int,int,int).
    type_constraints : list of list of two int vectors
        Each vector holds the indices of the domain and range constraints
        e.g.: [ [ndarray([0,1,2,3]),ndarray([4,5,6])] , [...,..] ]
    approximate_type_constraints : bool
        If True, type constraints are approximated based on the observed
        subject/object entities in each relation type.
    dtype : numpy dtype
        float dtype of the data in the tensor

    """
    def __init__(self, sparse_list, type_constraints=None,
                 approximate_type_constraints=False, dtype=np.float32):

        if type(sparse_list).__name__ == 'tuple':
            triples = sparse_list[0]
            shape = sparse_list[1]
            sparse_list = self.__initialize_from_triples(triples, shape)

        if type_constraints is None:
            type_constraints = [[None, None] for ti in sparse_list]

        self._sparse_list = sparse_list
        self._nnz = None
        self.type_constraints = type_constraints
        self.dtype = dtype
        self._row = None
        self._col = None
        self._fslice = None
        self._data = None
        self._size = None
        self._shape = None
        self._sparsity = None
        self._triples = None

    def tolil(self):
        return self.copy()

    def tocoo(self):
        return self.copy()

    def _initialize_from_triples(self, triples, shape):
        # remove dublicate indices
        triples = unique_rows(triples)

        # sort by frontal slice first
        sidx = np.argsort(triples[:, -1])
        triples = triples[sidx]
        slices = shape[2]
        shape = shape[:2]
        # build empty sparse list
        splist = [coo_matrix((np.array([]).astype(np.float32),
                              (np.array([]).astype(np.int32),
                               np.array([]).astype(np.int32))),
                             dtype=np.float32, shape=shape)
                  for i in xrange(slices)]

        # construct a list of coo_matrices
        current_slice = triples[0, -1]
        start_idx = 0
        for i in xrange(triples.shape[0]):
            if current_slice != triples[i, -1]:
                row = triples[start_idx:i, 0]
                col = triples[start_idx:i, 1]
                data = np.ones((len(row),), dtype=np.float32)
                splist[current_slice] = coo_matrix((data, (row, col)),
                                                   shape=shape,
                                                   dtype=np.float32)
                current_slice = triples[i, -1]
                start_idx = i
        else:
            row = triples[start_idx:, 0]
            col = triples[start_idx:, 1]
            data = np.ones((len(row),), dtype=np.float32)
            splist[current_slice] = coo_matrix((data, (row, col)), shape=shape,
                                               dtype=np.float32)

        return splist

    @property
    def nnz(self):
        return np.sum([ti.nnz for ti in self._sparse_list])

    @property
    def shape(self):
        return (self[0].shape[0], self[0].shape[1], len(self._sparse_list))

    @property
    def sparsity(self):
        return self.nnz/float(self.size)

    @property
    def size(self):
        """
        n*m*k if no type constraints are defined.
        """
        size = 0
        for t in self.type_constraints:
            nt = self.shape[0] if t[0] is None else len(t[0])
            mt = self.shape[1] if t[1] is None else len(t[1])
            size += nt * mt
        return size

    @property
    def row(self):
        if self.internal_type != 'coo_matrix':
            return np.hstack([ti.tocoo().row for ti in self._sparse_list])
        return np.hstack([ti.row for ti in self._sparse_list])

    @property
    def col(self):
        if self.internal_type != 'coo_matrix':
            return np.hstack([ti.tocoo().col for ti in self._sparse_list])
        return np.hstack([ti.col for ti in self._sparse_list])

    @property
    def fslice(self):
        if self.internal_type != 'coo_matrix':
            return np.hstack([np.ones((ti.nnz,),
                                      dtype=ti.tocoo().col.dtype) * i
                              for i, ti in enumerate(self._sparse_list)])
        return np.hstack([np.ones((ti.nnz,), dtype=ti.col.dtype) * i
                          for i, ti in enumerate(self._sparse_list)])

    @property
    def data(self):
        if self.internal_type != 'coo_matrix':
            return np.hstack([ti.tocoo().data for ti in self._sparse_list])
        return np.hstack([ti.data for ti in self._sparse_list])

    @property
    def triples(self):
        indices = np.empty((self.nnz, 3), dtype=np.int32)
        indices[:, 0] = self.row
        indices[:, 1] = self.col
        indices[:, 2] = self.fslice
        return indices

    def astype(self, dtype):
        self._sparse_list = [ti.astype(dtype) for ti in self._sparse_list]

    def copy(self):
        return TripleTensor([ti.copy() for ti in self._sparse_list],
                            self.type_constraints, dtype=self.dtype,
                            big_data=self.big_data)

    def is_zero(self, idx):
        return self[idx] == 0

    def agrees_with_tc(self, indices):
        """
        Checks wheter triples agree with the type-constraints of the tensor
        Parameter
        ---------
        indices : :classl:`numpy.ndarray`
            nx3 numpy array of ints, one triple index per row.
        """
        if indices.ndim == 1:
            indices = np.reshape(indices, (1, len(indices)))

        if indices.ndim == 2 and indices.shape[1] == 3:
            for i in xrange(indices.shape[0]):
                i, j, k = indices[i]
                if (i not in (np.arange(self.shape[0])
                              if self.type_constraints[k][0] is None
                              else self.type_constraints[k][0])):
                    # print "Disagree i:", i,j,k
                    return False
                if (j not in (np.arange(self.shape[1])
                              if self.type_constraints[k][1] is None
                              else self.type_constraints[k][1])):
                    # print "Disagree j:", i,j,k
                    return False
            return True
        else:
            raise Exception('Array must be of shape n x 3, but is of shape %d '
                            'x %d' % indices.shape)

    @abc.abstractmethod
    def __getitem__(self, index):
        return

    @abc.abstractmethod
    def __setitem__(self, index, x):
        return

    @abc.abstractmethod
    def __get_multiple_items(self, indices):
        return

    @abc.abstractmethod
    def __set_multiple_items(self, indices, x):
        return
