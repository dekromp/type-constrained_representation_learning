import numpy as np
import logging
from numbers import Number
import triple_tensor
import lil_triple_tensor

_log = logging.getLogger(__name__)


class CooTripleTensor(triple_tensor.TripleTensor):
    """
    Sparse 3D Tensor data structure that represents the tensor as a list of
    sparse coo_matrices as  frontal slices. Avoids memory expensive csr,csc
    and lil sparse matrix formats, what makes it applicable to very large
    triplestores.

    Parameters
    ----------
    sparse_list : list of scipy.sparse.coo_matrix instances or a nx3 ndarray
                  of indices (subject,object,predicate))
    type_constraints : list of list of two int vectors
        Each vector is holding the indices of the domain and range constraints
        e.g.: [ [ndarray([0,1,2,3]),ndarray([4,5,6])] , [...,..] ]
    approximate_type_constraints : bool
        If True, type constraints are approximated based on the observed
        subject/object entities in each relation type
        (Local closed-world assumption).
    dtype : numpy.dtype
        Float dtype of the data in the tensor.

    """
    def __init__(self, sparse_list, type_constraints=None,
                 approximate_type_constraints=False, dtype=np.float32):
        if type(sparse_list).__name__ == 'tuple':
            triples = sparse_list[0]
            shape = sparse_list[1]
            sparse_list = self._initialize_from_triples(triples, shape)

        for i in xrange(len(sparse_list)):
            if(sparse_list[i].shape[0] != sparse_list[0].shape[0] or
               sparse_list[i].shape[1] != sparse_list[0].shape[1]):
                raise(Exception('All matrices in the list must have the same'
                                'shape'))
            if type(sparse_list[0]).__name__ != 'coo_matrix':
                raise(Exception('All matrices have to be in '
                                'scipy.sparse.coo_matrix format'))
            sparse_list[i] = sparse_list[i].astype(dtype)

        if approximate_type_constraints:  # infer type_constraints
            type_constraints = [[np.unique(ti.row), np.unique(ti.col)]
                                for ti in sparse_list]

        if type_constraints is None:
            type_constraints = [[None, None] for ti in sparse_list]

        self.internal_type = 'coo_matrix'
        self._sparse_list = sparse_list
        self._nnz = None
        self.type_constraints = type_constraints
        self.dtype = dtype
        self._row = None
        self._col = None
        self._fslice = None
        self._data = None
        self._size = None
        self._shape = None
        self._sparsity = None
        self._triples = None

    def copy(self):
        return CooTripleTensor([ti.copy() for ti in self._sparse_list],
                               self.type_constraints, dtype=self.dtype)

    def tolil(self):
        return lil_triple_tensor.LilTripleTensor(
                    [ti.tolil() for ti in self._sparse_list],
                    self.type_constraints, dtype=self.dtype)

    def approximate_type_constraints(self):
        self.type_constraints = [[np.unique(ti.row), np.unique(ti.col)]
                                 for ti in self._sparse_list]

    def __getitem__(self, index):
        """
        Defines the indexing on the object instance
        """
        if isinstance(index, (int, np.int32, slice)):  # e.g.[1] or [1:5]
            return self._sparse_list[index]
        elif isinstance(index, tuple):  # e.g.[1,2,4] or [1:3,3:5,8]
            # convert to nx3 index array
            raise NotImplementedError
        # a n x 3 matrix of indices of n triples  (subject,object,predicate)
        elif (isinstance(index, (np.ndarray, np.memmap)) and
              index.ndim == 2 and index.shape[1] == 3 and
              isinstance(index[0, 0], (int, np.int32))):
            return self.__get_multiple_items(index)
        elif isinstance(index, (np.ndarray, np.memmap)) and index.ndim == 1:
            index = np.reshape(index, (1, len(index)))
            return self[index]
        else:
            raise NotImplementedError

    def __setitem__(self, index, x):
        """
        Assign Values to triples and add/delete triples
        Only deleting of triples implemented (=0)
        """
        if isinstance(index, (int, np.int32, slice)):  # e.g.[1] or [1:5]

            if isinstance(x, list):
                x = [xi.tocoo() for xi in x]
            else:
                x = x.tocoo()
            self._sparse_list[index] = x

        elif isinstance(index, tuple):  # e.g.[1,2,4] or [1:3,3:5,8]
            # convert to nx3 index array
            raise NotImplementedError
        # a n x 3 matrix of indices of n triples  (subject,object,predicate)
        elif (isinstance(index, (np.ndarray, np.memmap)) and
              index.ndim == 2 and index.shape[1] == 3 and
              isinstance(index[0, 0], (int, np.int32))):
            self.__set_multiple_items(index, x)
        else:
            raise NotImplementedError

    def __get_multiple_items(self, indices):
        """
        Get all values from indices
        """
        values = np.zeros((indices.shape[0],), dtype=np.float32)
        sidx = np.argsort(indices, axis=0)[:, 2]  # sort order of slices
        indices = indices[sidx]
        current = indices[0, 2]
        z = 0
        for i in xrange(indices.shape[0]):
            if current != indices[i, 2]:
                values[z:i] = self.__binary_coo_matrix_getter(indices[z:i])
                z = i
                current = indices[i, 2]
        else:
            values[z:] = self.__binary_coo_matrix_getter(indices[z:])
        indices = indices[np.argsort(sidx)]
        return values[np.argsort(sidx)]

    def __set_multiple_items(self, indices, x):
        """
        Setting multiple indices
        indices : nx3 ndarray of indices
        x : scalar or vector of values
        """
        sidx = np.argsort(indices, axis=0)[:, 2]  # sort order of slices
        indices = indices[sidx]
        current = indices[0, 2]
        z = 0
        if isinstance(x, Number):
            for i in xrange(indices.shape[0]):
                if current != indices[i, 2]:
                    self.__binary_coo_matrix_setter(indices[z:i], x)
                    z = i
                    current = indices[i, 2]
            else:
                self.__binary_coo_matrix_setter(indices[z:], x)
        else:
            raise NotImplementedError

        indices = indices[np.argsort(sidx)]

    def __binary_coo_matrix_getter(self, indices):
        k = indices[0, 2]
        n = len(self._sparse_list[k].row)
        dtype = self._sparse_list[k].row.dtype
        tmp = np.hstack([np.reshape(self._sparse_list[k].row, (n, 1)),
                         np.reshape(self._sparse_list[k].col, (n, 1))])
        return (
            np.in1d(indices[:, :2].copy().view([('', indices[:, :2].dtype)]*2),
                    tmp.copy().view([('', dtype)]*2)).astype(self.dtype))

    def __binary_coo_matrix_setter(self, indices, x):
        if x == 0:
            k = indices[0, 2]
            n = len(self._sparse_list[k].row)
            dtype = self._sparse_list[k].row.dtype
            tmp = np.hstack([np.reshape(self._sparse_list[k].row, (n, 1)),
                             np.reshape(self._sparse_list[k].col, (n, 1))])
            idcs = np.in1d(
                tmp.copy().view([('', dtype)]*2),
                indices[:, :2].copy().view([('', indices[:, :2].dtype)]*2))
            # inverse the boolean entries,  since we want all the unique
            # entries
            idcs = np.invert(idcs)

            # remove the triples from the coo_matrix
            self._sparse_list[k].row = self._sparse_list[k].row[idcs]
            self._sparse_list[k].col = self._sparse_list[k].col[idcs]
            self._sparse_list[k].data = self._sparse_list[k].data[idcs]
        else:
            raise NotImplementedError
