'''
This Module is used for splitting a triple dataset in a TripleTensor structure
for (cross-) validation purposes
'''

import numpy as np
from .sampling import shuffle_rows, compute_corrupted
import logging

_log = logging.getLogger(__name__)
DEF_RNG = np.random.RandomState(123456)


def split_random(X, split_ratios, rng=DEF_RNG, dtype=np.int32):
    """
    Splits the 3D tensor indices in n parts.
    Considers Type Constraints if included in X
    The split ratios have to sum up to one 1.
    Parameters:
    -----------
    X : :class:`~TripleTensor`
        The 3D tensor in TripleTensor Format.
    splits : :class:`numpy.ndarray`, list of floats or int
        The ratios of the splits or the amount of splits.
    rng : numpy.random.RandomState, optional
        Random number generator used for the split.
    """
    if np.sum(split_ratios) != 1 and type(split_ratios).__name__ == 'list':
        raise Exception("split ratios have to add up to 1")

    # get amount of indices sampled per frontal slice
    n_indices = 0
    for t in X.type_constraints:
        nt = X.shape[0] if t[0] is None else len(t[0])
        mt = X.shape[1] if t[1] is None else len(t[1])
        n_indices += nt * mt
    indices = np.empty((n_indices, 3), dtype=dtype)

    # sample all possible indices from the (type constrained) tensor
    z = 0
    for k, tc in enumerate(X.type_constraints):
        nk = X.shape[0] if tc[0] is None else len(tc[0])
        mk = X.shape[1] if tc[1] is None else len(tc[1])
        sh = nk*mk
        indices[z:z+sh, 0] = (
            np.repeat(np.arange(nk) if tc[0] is None else tc[0], mk).flatten())
        indices[z:z+sh, 1] = (
            np.repeat(np.reshape(np.arange(mk) if tc[1] is None
                                 else tc[1], (1, mk)), nk, axis=0).flatten())
        indices[z:z+sh, 2] = k
        z += sh

    # shuffle and split indices
    return __make_unique_shuffle_and_split_indices(indices, split_ratios, rng)


def partition_triples_with_nzeros(X, split_ratios, zeros_per_triple=1,
                                  sample_axes=[0, 1, 2], rng=DEF_RNG,
                                  allow_corrupted_ones=True, as_pairs=True,
                                  with_labels=True, dtype=np.int32):
    """
    Splits the triples in the 3D tensor into n parts and additionally sample m
    zero triples for each triple from the tensor. Considers Type Constraints if
    included in X. The split ratios have to sum up to one 1.
    RETURNS TRIPLES AS >>[SUBJECT,OBJECT,PREDICATE]<< INDICES!
    Parameters
    ----------
    X : :class:`~TripleTensor`
        The 3D tensor in TripleTensor Format.
    split_ratios : :class:`numpy-ndarray`, list of floats or int
        The ratios of the triples in each split.
    zeros_per_triple : int, optional
        The amount of random triples that are sampled per triple.
        Defaults to 1.
    sample_axes : list or :class:`numpy.ndarray`, optional
        Defines the way, the random (zero) triples are sampled for each true
        triple:
        [0] samples (zero) triples only from the same column (exchanges the
        subject).
        [1] samples (zero) triples only from the same row (exchanges the
        object).
        [2] exchanges the predicate.
        [0,1] selects any triple from the same relation type.
        [0,1,2], samples any (zero) triple from the tensor.
        Defaults to [0, 1, 2].
    rng : numpy.random.RandomState, optional
        Random number generator used for splitting.
    allow_corrupted_ones : bool
        whether or not it should be checked if the randomly sampled triples are
        all zeros.
        Defaults to True.
    as_pairs : bool, optional
        If True index arrays of shape nx6 is returned, where the first 3
        indices hold the true triple and the last 3 the corrupted one.
        Defaults to True.
    with_labels : bool, optional
        If True and as_pairs is False, attach the labels (0 or 1) as 4th
        column.
        Defaults to True.

    """
    # get all ones
    indices = np.empty((X.nnz, 3), dtype=dtype)
    indices[:, 0] = X.row
    indices[:, 1] = X.col
    indices[:, 2] = X.fslice

    # compute random zero triples
    if zeros_per_triple > 0:
        indices = compute_corrupted(X, indices, zeros_per_triple, sample_axes,
                                    allow_corrupted_ones, as_pairs=as_pairs,
                                    with_labels=with_labels, rng=rng)

    # shuffle and split indices
    return __make_unique_shuffle_and_split_indices(indices, split_ratios, rng)


def partition_triples(X, split_ratios, rng=DEF_RNG, dtype=np.int32):
    """
    Splits only the triples in the 3D tensor into n parts. Considers Type
    Constraints if included in X. The split ratios have to sum up to one 1.
    RETURNS TRIPLES AS >>[SUBJECT,OBJECT,PREDICATE]<< INDICES!
    Parameters
    ----------
    X : :class:`~TripleTensor`
        The 3D tensor in TripleTensor Format.
    split_ratios : :class:`numpyt.ndarray`, list of floats or int
        The ratios of the triples in each split.
    rng : numpy.random.RandomState
        Random number generator used for the splitting.

    """
    return partition_triples_with_nzeros(X, split_ratios, zeros_per_triple=0,
                                         rng=rng, allow_corrupted_ones=True,
                                         with_labels=False, dtype=np.int32)


def __make_unique_shuffle_and_split_indices(indices, split_ratios, rng):
    """
    Delete duplicate rows,
    shuffle the rows,
    and split the indices by the given ratios.
    Parameters
    ----------
    indices : :class:`numpy.ndarray`.
        A nx3 matrices which holds the triple indices.
    split_ratios : :class:`numpy.ndarray`, list of floats or int
        The ratios of the triples in each split.
    rng : numpy.random.RandomState
        Random number generator.

    """
    # remove duplicate rows
    indices = __unique_rows(indices)

    # shuffle and split indices
    indices = shuffle_rows(indices, rng)
    if(type(split_ratios).__name__ == 'list' or
       type(split_ratios).__name__ == 'ndarray'):
        return __split(indices, split_ratios)
    else:
        return np.array_split(indices, split_ratios)


def __split(indices, ratios):
    """
    Split the indices by the given ratios
    Parameters
    ----------
    indices : :class:`numpy.ndarray`
        A nx3 matrices which holds the triple indices.
    split_ratios : :class:`numpy.ndarray`, list of floats or int
        The ratios of the triples in each split.

    """
    s = np.append(0, np.cumsum(indices.shape[0] * np.array(ratios)))
    s = s.astype(indices.dtype)
    splits = []
    for i in xrange(1, len(s)):
        splits.append(indices[s[i-1]:s[i]])
    return splits


def __unique_rows(mat):
    """Deletes duplicate rows in a matrix.

    Parameters
    ----------
    indices : :class:`numpy.ndarray`
        A nx3 matrices which holds the triple indices.

    """
    try:
        unique_mat = np.unique(mat.view([('', mat.dtype)] * mat.shape[1]))
    except:
        _log.debug('Unique_rows raised an error, copying mat and trying '
                   'again!')
        mat = mat.copy()
        unique_mat = np.unique(mat.view([('', mat.dtype)]*mat.shape[1]))
    _log.debug('Removed %d duplicate indices from %d indices == %.2f!'
               % (mat.shape[0] - unique_mat.shape[0], mat.shape[0],
                  (mat.shape[0] - unique_mat.shape[0]) / float(mat.shape[0])))
    return unique_mat.view(mat.dtype).reshape((unique_mat.shape[0],
                                               mat.shape[1]))
