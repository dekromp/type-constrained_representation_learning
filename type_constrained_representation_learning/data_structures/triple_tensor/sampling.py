import numpy as np
import logging
import sys
import warnings
_log = logging.getLogger(__name__)
DEF_RNG = np.random.RandomState(123456)


def sample_random(X, size, remove_duplicates=False, rng=DEF_RNG,
                  dtype=np.int32):
    """
    Sample random triples from the 3D tensor (will contain unobserved triples).
    Considers Type Constraints if included in tensor.
    Parameters
    ----------
    X : :class:`~TripleTensor`
        The 3D tensor in TripleTensor Format
    size : int
        size of sample
    remove_duplicates : bool
        If True, duplicate triples are removed.
        This can lead to a smaller sample size
    rng : numpy.random.RandomState (optional)
        Random number generator

    """
    # get the relative sizes of each frontal slice to account for it during
    # sampling
    fslice_ratios = []
    for t in X.type_constraints:
        nt = X.shape[0] if t[0] is None else len(t[0])
        mt = X.shape[1] if t[1] is None else len(t[1])
        fslice_ratios.append(nt*mt)
    fslice_ratios = (np.array(fslice_ratios).astype(np.float64) /
                     np.sum(np.array(fslice_ratios)))

    # sample the frontal slice indices
    ks = rng.choice(X.shape[2], size=(size,), p=fslice_ratios)
    ks = np.sort(ks)

    # aample from the frontal slices
    indices = np.empty((size, 3), dtype=dtype)
    current = ks[0]
    z = 0
    for i in xrange(len(ks)):
        if ks[i] != current:
            indices[z:i, 0] = (
                rng.choice(X.shape[0]
                           if X.type_constraints[current][0] is None
                           else X.type_constraints[current][0], i - z))
            indices[z:i, 1] = (
                rng.choice(X.shape[1]
                           if X.type_constraints[current][1] is None
                           else X.type_constraints[current][1], i - z))
            indices[z:i, 2] = current
            z = i
            current = ks[i]
    else:
        indices[z:, 0] = (
            rng.choice(X.shape[0]
                       if X.type_constraints[current][0] is None
                       else X.type_constraints[current][0], len(ks) - z))
        indices[z:, 1] = (
            rng.choice(X.shape[1]
                       if X.type_constraints[current][1] is None
                       else X.type_constraints[current][1], len(ks) - z))
        indices[z:, 2] = current

    return (shuffle_rows(unique_rows(indices), rng)
            if remove_duplicates else shuffle_rows(indices, rng))


def sample_triples_like_bordes(X, size, zeros_per_triple=1,
                               remove_duplicates=False, rng=DEF_RNG,
                               as_pairs=True, with_labels=True,
                               dtype=np.int32):
    """
    Sampling is performed as done by Bordes et al in their Translational
    Embeddings Model, where subject and objects are corrupted but not both at
    the same time. In contrast to their sampling, this sampling considers type-
    constraints if included in X.
    Parameters:
    -----------
    X : :class:`~TripleTensor`
        The 3D tensor in TripleTensor Format
    size : int
        Amount of true triples sampled
    zeros_per_triple : int (optional)
        The amount of random triples that are sampled per triple.
        Defaults to 1.
    remove_duplicates : bool, optional
        If True, duplicate triples are removed.
        This can lead to a smaller sample size.
        Defaulta to False.
    rng : numpy.random.RandomState, optional
        Random number generator.
    as_pairs : bool, optional
        If True an index array of shape n x 6 is returned, where the first 3
        indices hold the true triple and the last 3 the corrupted one. If
        False, size+size*zeros_per_triple triples are returned.
        Defaults to `True`.
    with_labels : bool, optional
        If True and as_pairs is False, attach the labels (0 or 1) as 4th
        column.
        Defaults to `True`.
    """
    # get random triples
    indices = np.empty((size, 3), dtype=dtype)
    rnd_idcs = rng.choice(X.nnz, size, replace=False)
    indices[:, 0] = X.row[rnd_idcs]
    indices[:, 1] = X.col[rnd_idcs]
    indices[:, 2] = X.fslice[rnd_idcs]

    # compute random zero triples
    if zeros_per_triple > 0:
        indices = (
            compute_corrupted_bordes(X, indices,
                                     corrupted_per_triple=zeros_per_triple,
                                     as_pairs=as_pairs,
                                     with_labels=with_labels, rng=rng))

    return (shuffle_rows(unique_rows(indices), rng)
            if remove_duplicates else shuffle_rows(indices, rng))


def sample_triples_with_nzeros(X, size, zeros_per_triple=1,
                               sample_axes=[0, 1, 2],
                               remove_duplicates=False, rng=DEF_RNG,
                               allow_corrupted_ones=True, as_pairs=True,
                               with_labels=True, dtype=np.int32):
    """
    Randomly samples triples from the 3D tensor and additionally
    samples m negative triples for each of those triples.
    Considers Type Constraints if included in X
    Parameters
    ----------
    X : :class:`~TripleTensor`
        The 3D tensor in TripleTensor Format
    size : int
        Amount of true triples sampled.
    zeros_per_triple : int, optional
        The amount of random triples that are sampled per triple.
        Defaults to 1.
    sample_axes : list or :class:`numpy.ndarray`, optional
        Defines the way, the random (zero) triples are sampled for each true
        triple. [0] samples (zero) triples only from the same column (exchanges
        the subject)
        [1] samples (zero) triples only from the same row (exchanges the
        object).
        [2] exchanges the predicate.
        [0,1] selects any triple from the same relation type.
        [0,1,2], samples any (zero) triple from the tensor.
        Defaults to [0,1,2].
    remove_duplicates : bool, optional
        If True, duplicate triples are removed. This can lead to a smaller
        sample size.
        Defaults to False.
    rng : numpy.random.RandomState, optional
        Random number generator used for sampling.
    allow_corrupted_ones : bool, optional
        Whether or not it should be checked if the randomly sampled triples are
        all zeros.
        Defaults to True.
    as_pairs : bool, optional
        If True an index array of shape nx6 is returned, where the first 3
        indices hold the true triple and the last 3 the corrupted one. If
        False, size+size*zeros_per_triple  triples are returned.
        Defaults to True.
    with_labels : bool, optional
        If True and as_pairs is False, attach the labels (0 or 1) as 4th
        column.
        Defaults to True.

    """
    # get random triples
    indices = np.empty((size, 3), dtype=dtype)
    rnd_idcs = rng.choice(X.nnz, size, replace=False)
    indices[:, 0] = X.row[rnd_idcs]
    indices[:, 1] = X.col[rnd_idcs]
    indices[:, 2] = X.fslice[rnd_idcs]

    # compute random zero triples
    if zeros_per_triple > 0:
        indices = compute_corrupted(X, indices, zeros_per_triple, sample_axes,
                                    allow_corrupted_ones, as_pairs=as_pairs,
                                    with_labels=with_labels, rng=rng)

    return (shuffle_rows(unique_rows(indices), rng)
            if remove_duplicates else shuffle_rows(indices, rng))


def sample_triples(X, size, rng=DEF_RNG, dtype=np.int32):
    """
    Randomly samples triples from the 3D tensor.
    Considers Type Constraints if included in X.

    Parameters
    ----------
    X : :class:`~TripleTensor`
        The 3D tensor in TripleTensor Format
    size : int
        Amount of true triples sampled
    rng : numpy.random.RandomState, optional
        Random number generator used for sampling.
    """
    return sample_triples_with_nzeros(X, size, zeros_per_triple=0,
                                      remove_duplicates=False,
                                      with_labels=False, rng=rng, dtype=dtype)


def shuffle_rows(mat, rng=DEF_RNG):
    """
    Is 3xfaster than np.random.shuffle... why?
    Returns the matrix with permuted rows.
    Parameters
    ----------
    mat : 2D numpy array
        Matrix that is shuffled
    rng : numpy.random.RandomState (optional)
        Random number generator

    """
    idx = np.arange(mat.shape[0])
    rng.shuffle(idx)
    return mat[idx]


def unique_rows(mat):
    """
    Deletes duplicate rows in a sparse matrix.
    Parameters
    ----------
    mat : :class:`numpy.ndarray`
        A 2D numpy array.

    """
    try:
        unique_mat = np.unique(mat.view([('', mat.dtype)]*mat.shape[1]))
    except:
        _log.debug('Unique_rows raised an error, copying mat and trying '
                   'again!')
        mat = mat.copy()
        unique_mat = np.unique(mat.view([('', mat.dtype)]*mat.shape[1]))
    return unique_mat.view(mat.dtype).reshape((unique_mat.shape[0],
                                               mat.shape[1]))


def compute_corrupted_bordes(X, indices, corrupted_per_triple=1,
                             sample_axes=None, allow_corrupted_ones=None,
                             as_pairs=False, with_labels=True, rng=DEF_RNG):
    """
    Compute corrupted indices for each index in indices
    Parameters
    ----------
    X : :class:`~TripleTensor`
        The 3D tensor in TripleTensor Format.
    indices : 2D numpy array
        n x 3 matrix that contains the triple indices.
    corrupted_per_triple : int, optional
        Number of corrupted to sample per triple (row in indices) where for one
        corrupted one triple where the subject is corrupted and one triple
        where the object is corrupted are sampled.
        Defaults to 1.
    as_pairs : bool, optional
        If True an index array of shape nx6 is returned, where the first 3
        indices hold the true triple and the last 3 the corrupted one. If
        False, size+size*zeros_per_triple  triples are returned.
        Defaults to False.
    with_labels : bool, optional
        If True and as_pairs is False, attach the labels (0 or 1) as 4th
        column.
        Defaults to True.
    rng : numpy.random.RandomState, optional
        Random number generator used for sampling corrupted.

    """
    cor_idcs = np.repeat(indices, 2 * corrupted_per_triple + 1, axis=0)
    for i in xrange(1, cor_idcs.shape[0], 2 * corrupted_per_triple + 1):
        k = cor_idcs[i, 2]
        # sample corrupted entities with or without type constraints
        # corrupt the subject
        if X.type_constraints[k][0] is None:
            cor_idcs[i + np.arange(0, 2 * corrupted_per_triple, 2), 0] = (
                rng.choice(X.shape[0], size=(corrupted_per_triple,)))
        else:
            cor_idcs[i + np.arange(0, 2 * corrupted_per_triple, 2), 0] = (
                rng.choice(X.type_constraints[k][0],
                           size=(corrupted_per_triple,)))

        # corrupt the objects
        if X.type_constraints[k][1] is None:
            cor_idcs[i + np.arange(1, 2 * corrupted_per_triple, 2), 1] = (
                rng.choice(X.shape[1], size=(corrupted_per_triple,)))
        else:
            cor_idcs[i + np.arange(1, 2 * corrupted_per_triple, 2), 1] = (
                rng.choice(X.type_constraints[k][1],
                           size=(corrupted_per_triple,)))

    if as_pairs:
        corrupted = np.setdiff1d(np.arange(cor_idcs.shape[0]),
                                 np.arange(0, cor_idcs.shape[0],
                                           2 * corrupted_per_triple + 1))
        cor_idcs = np.hstack([np.repeat(indices, 2 * corrupted_per_triple,
                                        axis=0),
                              cor_idcs[corrupted]])
    elif with_labels:
        corrupted = np.setdiff1d(np.arange(cor_idcs.shape[0]),
                                 np.arange(0, cor_idcs.shape[0],
                                           2 * corrupted_per_triple + 1))
        labels = np.ones((cor_idcs.shape[0], 1), dtype=np.int32)
        labels[corrupted] = 0
        cor_idcs = np.hstack([cor_idcs, labels])

    return cor_idcs


def compute_corrupted(X, indices, corrupted_per_triple, sample_axes,
                      allow_corrupted_ones, as_pairs, with_labels=True,
                      rng=DEF_RNG):
    """
    Compute corrupted indices for each index in indices.
    Parameters
    ----------
    X : :class:`~TripleTensor`
        The 3D tensor in TripleTensor Format.
    indices : :class:`numpy.ndarray`
        nx3 matrix that contains the triple indices.
    corrupted_per_triple : int
        Number of corrupted to sample per triple (row in indices)
    sample_axes : list or numpy array
        Defines the way, the random (zero) triples are sampled for each true
        triple:
        [0] samples (zero) triples only from the same column (exchanges the
        subject).
        [1] samples (zero) triples only from the same row (exchanges the
        object).
        [2] exchanges the predicate (might dramatically increase sampling time
        with type-constraints).
        [0,1] selects any triple from the same relation type.
        [0,1,2], samples any (zero) triple from the tensor.
    allow_corrupted_ones : bool
        Whether or not it should be checked if the randomly sampled
        triples are all zeros.
    as_pairs : bool
        If True an index array of shape nx6 is returned, where the first 3
        indices.
        hold the true triple and the last 3 the corrupted one
        If False, size+size*zeros_per_triple  triples are returned
    with_labels : bool, optional
        If True and as_pairs is False, attach the labels (0 or 1) as 4th
        column.
        Defaults to True.
    rng : numpy.random.RandomState, optional
        Random number generator used for sampling corrupted triples.

"""
    cor_idcs = np.repeat(indices, corrupted_per_triple + 1, axis=0)
    for i in xrange(1, cor_idcs.shape[0], corrupted_per_triple + 1):
        k = cor_idcs[i, 2]
        for ax in sample_axes:
            if ax == 2:  # sample corrupted predicates
                cor_idcs[i:i+corrupted_per_triple, ax] = (
                    rng.choice(X.shape[ax], size=(corrupted_per_triple,)))
                # since it cannot be guaranteed, that a changed predicate
                # results in a valid triple (type constraints).
                # check if the indices are valid and try to resample if not
                for j in xrange(corrupted_per_triple):
                    if not X.agrees_with_tc(cor_idcs[i+j, :]):
                        cor_idcs[i+j] = resample_tc(X, cor_idcs[i+j],
                                                    cor_idcs[i-1],
                                                    sample_axes, rng=rng)
            else:  # sample corrupted entities with or without type constraints
                if X.type_constraints[k][ax] is None:
                    cor_idcs[i:i+corrupted_per_triple, ax] = (
                        rng.choice(X.shape[ax], size=(corrupted_per_triple,)))
                else:
                    cor_idcs[i:i+corrupted_per_triple, ax] = (
                        rng.choice(X.type_constraints[k][ax],
                                   size=(corrupted_per_triple,)))

    if not allow_corrupted_ones:
        # replace corrupted triples that are ones with ones that are real zeros
        cidcs = np.setdiff1d(np.arange(cor_idcs.shape[0]),
                             np.arange(0, cor_idcs.shape[0],
                                       corrupted_per_triple + 1))
        for t in xrange(10):
            if len(cidcs) > 0:
                iszero = X.is_zero(cor_idcs[cidcs, :])
                cidcs = cidcs[np.where(iszero is False)[0]]
                cor_idcs[cidcs, :] = resample_false_ones(X, cor_idcs[cidcs],
                                                         sample_axes, rng=rng)

        if len(cidcs) > 0:
            warnings.warn('It was not possible to resample the indices %s'
                          % (str(cor_idcs[cidcs])))

    if as_pairs:
        corrupted = np.setdiff1d(np.arange(cor_idcs.shape[0]),
                                 np.arange(0, cor_idcs.shape[0],
                                           corrupted_per_triple + 1))
        cor_idcs = np.hstack([np.repeat(indices, corrupted_per_triple, axis=0),
                              cor_idcs[corrupted]])
    elif with_labels:
        corrupted = np.setdiff1d(np.arange(cor_idcs.shape[0]),
                                 np.arange(0, cor_idcs.shape[0],
                                           corrupted_per_triple + 1))
        labels = np.ones((cor_idcs.shape[0], 1), dtype=np.int32)
        labels[corrupted] = 0
        cor_idcs = np.hstack([cor_idcs, labels])

    return cor_idcs


def resample_false_ones(X, idcs, axes, rng=DEF_RNG):
    """
    Resample an corrupted index that is a true triple
    with respect to all sample axes.
    Parameters
    ----------
    X : :class:`~TripleTensor`
        The 3D tensor that holds the type constraints.
    idx : :class:`numpy.ndarray`
        Index vector that has to be resampled.
    axes : list or :class:`numpy.ndarray`
        Defines the way, the random (zero) triples are sampled for each true
        triple:
        [0] samples (zero) triples only from the same column (exchanges the
        subject).
        [1] samples (zero) triples only from the same row (exchanges the
        object).
        [2] exchanges the predicate (might dramatically increase sampling time
        with type-constraints).
        [0,1] selects any triple from the same relation type.
        [0,1,2], samples any (zero) triple from the tensor.
    rng : numpy.random.RandomState, optional
        Random number generator used for resampling triples.

    """
    backup_idcs = idcs.copy()
    for i in xrange(idcs.shape[0]):
        for ax in axes:
            if ax < 2 and X.type_constraints[idcs[i, 2]][ax] is not None:
                idcs[i, ax] = rng.choice(X.type_constraints[idcs[i, 2]][ax],
                                         size=1)
            else:
                idcs[i, ax] = rng.choice(X.shape[ax], size=1)
                if(ax == 2 and
                   X.type_constraints[idcs[i, 2]][0] is not None and
                   X.type_constraints[idcs[i, 2]][1] is not None):
                    if not X.agrees_with_tc(idcs[i]):
                        idcs[i, :] = resample_tc(X, idcs[i], backup_idcs[i],
                                                 axes, rng=rng)
    return idcs


def resample_tc(X, idx, backup_idx, axes, tries=10, rng=DEF_RNG):
    """
    Resample an index that disagrees with type constraints
    with respect to all sample axes.
    Parameters
    ----------
    X : :class:`~TripleTensor`
        The 3D tensor that holds the type constraints
    idx : :class:`numpy.ndarray`
        Index vector that has to be resampled.
    axes : list or :class:`numpy.ndarray`
        Defines the way, the random (zero) triples are sampled for each true
        triple:
        [0] samples (zero) triples only from the same column (exchanges the
        subject).
        [1] samples (zero) triples only from the same row (exchanges the
        object).
        [2] exchanges the predicate (might dramatically increase sampling time
        with type-constraints).
        [0,1] selects any triple from the same relation type.
        [0,1,2], samples any (zero) triple from the tensor.
    backup_idx : :class:`numpy.ndarray`
        Index vector that is used in case the resampling fails.
    func : callable
        Function used to decide if an index (triple) is valid.
    tries : int, optional
        Number of trials to sample a correct index.
        Defaults to 10.
    rng : numpy.random.RandomState, optional
        Random number generator used for resampling triples.

    """
    for t in xrange(tries):
        for ax in axes:
            if ax < 2 and X.type_constraints[idx[2]][ax] is not None:
                idx[ax] = rng.choice(X.type_constraints[idx[2]][ax], size=1)
            else:
                idx[ax] = rng.choice(X.shape[ax], size=1)

            if X.agrees_with_tc(idx):
                return idx

    warnings.warn('It was not possible to resample the index %d %d %d'
                  % tuple(idx))
    return backup_idx
