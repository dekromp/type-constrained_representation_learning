import unittest
import sys
from type_constrained_representation_learning.data_structures\
    .triple_tensor import *
import numpy as np
from scipy.sparse import lil_matrix
from type_constrained_representation_learning.data_structures\
    .triple_tensor import LilTripleTensor
from type_constrained_representation_learning.data_structures\
    .triple_tensor import CooTripleTensor
from type_constrained_representation_learning.data_structures.triple_tensor\
    .validate import validate_on_triple_tensor
from type_constrained_representation_learning.data_structures.triple_tensor\
    .validate import validate_on_triple_tensor_single
from type_constrained_representation_learning.models import RandomTensorModel
DEF_SEED = 1234565


class Test(unittest.TestCase):
    def setUp(self):
        ### create the test sparse_matrix_list
        n,m,k = 7,5,6
        self.test_tensors = []
        self.seed = 100000#np.random.randint(100000000)
        self.test_tensors.append(random_splist_without_tc(n,m,k,self.seed,constructor="LilTripleTensor"))
        self.test_tensors.append(random_splist_without_tc(n,m,k,self.seed,constructor="CooTripleTensor"))
        
        self.test_tensors.append(random_splist_with_approx_tc(n,m,k,self.seed,constructor="LilTripleTensor"))
        self.test_tensors.append(random_splist_with_approx_tc(n,m,k,self.seed,constructor="CooTripleTensor"))
        
        self.test_tensors.append(random_splist_with_tc_mix(n,m,k,self.seed,constructor="LilTripleTensor"))
        self.test_tensors.append(random_splist_with_tc_mix(n,m,k,self.seed,constructor="CooTripleTensor"))
        
        #for ti in self.test_tensors:
        #    print type(ti).__name__
        
        self.indices = np.empty((10,3),dtype=np.int32)
        rng = np.random.RandomState(self.seed)
        self.indices[:,0] = rng.choice(n,10)
        self.indices[:,1] = rng.choice(m,10)
        self.indices[:,2] = rng.choice(k,10)
      
    ### TEST SPARSE_MATRIX_LIST DATA STRUCTURE ###
    def testMemoryEfficientGetter(self):
        print "Testing Memory Efficient Getter"
        for i,ti in enumerate(self.test_tensors):
            sm = None
            if isinstance(ti, CooTripleTensor):
                sm = LilTripleTensor([t.tolil() for t in ti[:]], ti.type_constraints,ti.dtype)
            else:
                sm = CooTripleTensor([t.tocoo() for t in ti[:]], ti.type_constraints,ti.dtype)
    
            self.assertTrue(np.sum(sm[self.indices]-ti[self.indices]) == 0)
    
    def testInitializationFromTripes(self):
        print "Testing Initialization from Triples",
        for i,ti in enumerate(self.test_tensors):
            triples = ti.triples
            shape = ti.shape
            tensor = eval(type(ti).__name__)((triples,shape),type_constraints=ti.type_constraints,dtype=ti.dtype)
            
            ### Test High level properties
            self.assertEqual(tensor.nnz,ti.nnz, "Expected nnz of %d but observed %d"%(ti.nnz,tensor.nnz))
            self.assertEqual(tensor.size,ti.size, "Expected size of %d but observed %d"%(ti.size,tensor.size))
            self.assertEqual(tensor.shape,ti.shape, "Expected shape (%d,%d,%d) but observed (%d,%d.%d)"%(ti.shape[0],ti.shape[1],ti.shape[2],tensor.shape[0],tensor.shape[0],tensor.shape[2]) )
            
            ### Test content
            for k in xrange(tensor.shape[2]):
                self.assertEqual((tensor[k]-ti[k]).nnz,0,"Expected nnz of 0 but observed %d"%(tensor[k]-ti[k]).nnz)
                
        print "OK"
    
    
    def testIndexing(self):
        print "Testing Indexing of SparseMatrixList",
        np.random.seed(self.seed)
        idcs = np.empty((5,3),dtype=np.int32)
        idcs[:,0] = np.random.choice(self.test_tensors[0].shape[0],idcs.shape[0])
        idcs[:,1] = np.random.choice(self.test_tensors[0].shape[1],idcs.shape[0])
        
        for k in xrange(self.test_tensors[0].shape[2]):
            idcs[:,2] = k
            for i in xrange(0,len(self.test_tensors),2):
                ti = self.test_tensors[i]
                tmp1 = ti[k].copy().tolil()
                tmp2 = ti.copy()
                tmp1[idcs[:,0],idcs[:,1]] = 4
                tmp2[idcs] = 4
                
                # test if setting by index is correct
                self.failUnless(np.sum(tmp1.todense() - tmp2[k].todense()) == 0 )
                self.failIf(np.sum(tmp1.todense() - ti[k].todense()) == 0 )
                # test if getting by index is correct
                self.failUnless(np.sum(tmp1[idcs[:,0],idcs[:,1]] - tmp2[idcs]) == 0)
                self.failIf(np.sum(np.zeros((idcs.shape[0],)) - tmp2[idcs]) == 0)
        print "OK"
    
    ### TEST SPLITTING ###
    def testSplitRandom(self):
        print "Testing Random Splitting of SparseMatrixList..."
        rng = np.random.RandomState(self.seed)
        split_ratios = [0.5,.2,.3]
        for z,ti in enumerate(self.test_tensors):
            splits = split_random(ti,split_ratios,rng, dtype=np.int32) #list input
            splits2 = split_random(ti,len(split_ratios),rng, dtype=np.int32) #int input
            # test amount of splits
            print "Test Tensor %d: Amount of Splits"%z,
            self.failUnless(len(splits) == len(split_ratios) )
            self.failUnless(len(splits2) == len(split_ratios) )
            print "OK"
            # test size of splits
            print "Test Tensor %d: Size of Splits"%z,
            for s,ratio in enumerate(split_ratios):
                self.failUnless(abs(ratio*ti.size - splits[s].shape[0]) <1) #due to uneven splits
                self.failUnless(abs(1./len(split_ratios)*ti.size - splits2[s].shape[0]) <1) #due to uneven splits
            print "OK"
            # test if all indices occur in the splits
            print "Test Tensor %d: If splits cover all indices, also considering Type Constraints"%z,
            all_idx = np.empty((ti.size,3),dtype=np.int32);z=0
            for k in xrange(ti.shape[2]):
                for i in np.arange(ti.shape[0]) if ti.type_constraints[k][0] is None else ti.type_constraints[k][0]:
                    for j in np.arange(ti.shape[1]) if ti.type_constraints[k][1] is None else ti.type_constraints[k][1]:
                        all_idx[z] = i,j,k; z+=1
            split_idcs = np.vstack(splits)
            sidx = sorted(np.arange(split_idcs.shape[0]), key=lambda x : (split_idcs[x,2], split_idcs[x,0], split_idcs[x,1]) )
            split_idcs[sidx]
            self.failUnless(np.sum(all_idx-split_idcs)==0)
            split_idcs = np.vstack(splits2)
            sidx = sorted(np.arange(split_idcs.shape[0]), key=lambda x : (split_idcs[x,2], split_idcs[x,0], split_idcs[x,1]) )
            split_idcs[sidx]
            self.failUnless(np.sum(all_idx-split_idcs)==0)
            print "OK"
            
    def testComputeCorruptedBordes(self):        
        print "Testing Sampling of Corrupted Triples like Bordes did..."
        corrupted = 3
        for z,ti in enumerate(self.test_tensors):
            indices = np.empty((ti.nnz,3),dtype=np.int32)
            indices[:,0] = ti.row[:ti.nnz]
            indices[:,1] = ti.col[:ti.nnz]
            indices[:,2] = ti.fslice[:ti.nnz]
            rng = np.random.RandomState(self.seed)
            #test corrupted computation with sampling  along all axes
            print "Test Tensor %d: Allow Corrupted Ones, corrupted fall into type constraints,"%z,
            cor012 = compute_corrupted_bordes(ti, indices, corrupted_per_triple=corrupted, as_pairs=False, with_labels=False, rng=np.random.RandomState(self.seed))
            self.failUnless(ti.agrees_with_tc(cor012))
            self.failUnless(cor012.shape[0] == indices.shape[0]*(2*corrupted+1))
            self.failUnless(unique_rows(cor012).shape[0] > unique_rows(indices).shape[0]) 
            print "OK"
            
            #check return as pairing
            print "Test Tensor %d: Same works for paired output,"%z,
            rng = np.random.RandomState(self.seed)
            cor012_b = compute_corrupted_bordes(ti, indices, corrupted_per_triple=corrupted, as_pairs=True, rng=np.random.RandomState(self.seed))   
            self.failUnless(np.sum(np.abs(indices-cor012_b[np.arange(0,cor012_b.shape[0],2*corrupted),:3])) == 0)
            idcs = np.setdiff1d(np.arange(cor012.shape[0]), np.arange(0,cor012.shape[0],2*corrupted+1))
            self.failUnless(np.sum(np.abs(cor012[idcs]-cor012_b[:,3:6])) == 0)
            print "OK"
            
    def testComputeCorrupted(self):
        print "Testing Sampling of Corrupted Triples (Sampling along all axes)..."
        corrupted = 2
        for z,ti in enumerate(self.test_tensors):
            indices = np.empty((ti.nnz,3),dtype=np.int32)
            indices[:,0] = ti.row[:ti.nnz]
            indices[:,1] = ti.col[:ti.nnz]
            indices[:,2] = ti.fslice[:ti.nnz]
            rng = np.random.RandomState(self.seed)
            
            #test corrupted computation with sampling  along all axes
            print "Test Tensor %d: Allow Corrupted Ones, corrupted fall into type constraints,"%z,
            cor012 = compute_corrupted(ti, indices, corrupted, [0,1,2], allow_corrupted_ones=True, as_pairs=False, with_labels=False, rng=np.random.RandomState(self.seed))
            self.failUnless(ti.agrees_with_tc(cor012))
            self.failUnless(cor012.shape[0] == indices.shape[0]*(corrupted+1))
            self.failUnless(unique_rows(cor012).shape[0] > unique_rows(indices).shape[0]) 
            print "OK"
            
            #check allow corrupted option=False
            print "Test Tensor %d: Don't allow corrupted triples to be ones,"%z,
            rng = np.random.RandomState(self.seed)
            cor012_a = compute_corrupted(ti, indices, corrupted, [0,1,2], allow_corrupted_ones=False, as_pairs=False, with_labels=False, rng=np.random.RandomState(self.seed))
            self.assertTrue(unique_rows(cor012_a).shape[0] >= unique_rows(cor012).shape[0], "Shape %d is not >= Shape %d"%(unique_rows(cor012_a).shape[0],unique_rows(cor012).shape[0]))
            print "OK"
            
            #check return as pairing
            print "Test Tensor %d: Same works for paired output,"%z,
            rng = np.random.RandomState(self.seed)
            cor012_b = compute_corrupted(ti, indices, corrupted, [0,1,2], allow_corrupted_ones=True, as_pairs=True, rng=np.random.RandomState(self.seed))   
            self.failUnless(np.sum(np.abs(indices-cor012_b[np.arange(0,cor012_b.shape[0],corrupted),:3])) == 0)
            idcs = np.setdiff1d(np.arange(cor012.shape[0]), np.arange(0,cor012.shape[0],corrupted+1))
            self.failUnless(np.sum(np.abs(cor012[idcs]-cor012_b[:,3:6])) == 0)
            
            #check return as pairing with allow corrupted option=False
            rng = np.random.RandomState(self.seed)
            cor012_c = compute_corrupted(ti, indices, corrupted, [0,1,2], allow_corrupted_ones=False, as_pairs=True, rng=np.random.RandomState(self.seed))   
            self.failUnless(np.sum(np.abs(indices-cor012_c[np.arange(0,cor012_c.shape[0],corrupted),:3])) == 0)
            idcs = np.setdiff1d(np.arange(cor012_a.shape[0]), np.arange(0,cor012_a.shape[0],corrupted+1))
            self.failUnless(np.sum(np.abs(cor012_a[idcs]-cor012_c[:,3:6])) == 0)
            print "OK"
            
    ### TEST SAMPLING ###
    def testSampleRandom(self):
        print "Testing Random Splitting of SparseMatrixList..."
        rng = np.random.RandomState(self.seed)
        sample_size = 100
        for z,ti in enumerate(self.test_tensors):
            print "Test Tensor %d: Check size of sample with and without eliminating duplicates,"%z,
            sample = sample_random(ti,sample_size,remove_duplicates=False,rng=rng)
            self.failUnless(sample.shape[0] == sample_size)
            self.failUnless(unique_rows(sample).shape[0] > 2 )
            
            sample2 = sample_random(ti,sample_size,remove_duplicates=True,rng=rng)
            self.failUnless(sample2.shape[0] < sample.shape[0])
            print "OK"
            
            print "Test Tensor %d: Check if Type Constraints are considered"%z,
            self.failUnless(ti.agrees_with_tc(sample))
            self.failUnless(ti.agrees_with_tc(sample2))
            print "OK"
    
    ### TEST VALIDATION ###
    def testValidateOnSparseMatricList(self):
        print "Test validate_on_triple_tensor"
        models = [RandomTensorModel(self.seed),
                  RandomTensorModel(self.seed),
                  RandomTensorModel(self.seed)]
        for z,ti in enumerate(self.test_tensors):
            splits_complete = split_random(ti, [0.2,0.8], np.random.RandomState(self.seed))
            splits_triples = partition_triples_with_nzeros(ti, [0.2,0.8], 
                                                           zeros_per_triple=3, 
                                                           sample_axes=[0,1,2], 
                                                           rng=np.random.RandomState(self.seed),
                                                           allow_corrupted_ones=False, 
                                                           as_pairs=False, with_labels=False)
            
    
            scst1, sc1 = validate_on_triple_tensor(ti,splits_complete,models)
            scst2, sc2 = validate_on_triple_tensor(ti,splits_triples,models)
            print "Test Tensor %d: All model scores have been computed. " % z,
            self.failUnless(len(scst1[scst1==0]) == 0)
            self.failUnless(len(scst2[scst2==0]) == 0)
            self.failUnless(len(sc1[sc1==0]) == 0)
            self.failUnless(len(sc2[sc2==0]) == 0)
            print "OK"
        
    
    def testApproximateTypeConstraints(self):
        for z,ti in enumerate(self.test_tensors):
            t1 = ti.tolil()
            t1.approximate_type_constraints()
            t1 = t1.type_constraints
            t2 = ti.tocoo()
            t2.approximate_type_constraints()
            t2 = t2.type_constraints
            self.failUnless(len(t1)==len(t2))
            for i in xrange(len(t1)):
                if t1[i][0] is not None:
                    self.failUnless(np.sum(t1[i][0]-t2[i][0])==0)
                if t1[i][1] is not None:
                    self.failUnless(np.sum(t1[i][1]-t2[i][1])==0)

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testApproximateTypeConstraints']
    unittest.main()