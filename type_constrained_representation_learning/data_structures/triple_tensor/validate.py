import joblib
import logging
import multiprocessing as mp
import numpy as np
import os
from scipy.sparse import csr_matrix
import shutil
import time
import warnings

from ...experiments.score_visualization import bars_with_scores
from ...experiments.metrics import auprc
from ...experiments.metrics import auroc
_log = logging.getLogger('validate_on_sparse_list_data')


def validate_on_triple_tensor_single(tensor, splits, models,
                                     metrics=[auprc, auroc]):
    """
    Perform (cross)-validation on a dataset that has a (Coo/Lil)TripleTensor
    data structure (Single-Threaded).
    Parameters
    ----------
    tensor : :class:`~TripleTensor`
         The 3D (triple) tensor.
    models : list of :class:`~models`
         Latent Variable Models that learn based on indices.
    splits :  list of :class:`numpy.ndarray`
        nx3 numpy arrays, where each array contains the indices, that are set
        to zero and evaluated against after training.
    metrics : list of callable, optional
        Functions that compute an evaluation metric.
        Defaults to [auprc, auroc].

    """
    # auprc, auroc scores ...
    scores = np.zeros((len(metrics), len(models), len(splits)))
    # mean and std
    scores_stats = np.zeros((len(metrics), len(models), 2))
    traintimes = np.zeros((len(models),))

    for i, test_idcs in enumerate(splits):
        # get values of testset and remove them from the tensor to get the
        # trainset
        y_test = tensor[test_idcs]
        X_train = tensor.copy()
        X_train[test_idcs] = 0
        _log.info('Size Testset: %d, NNZ Testset: %d, NNZ Trainset: %d, NNZ'
                  ' Complete: %d' % (len(y_test), csr_matrix(y_test).nnz,
                                     X_train.nnz, tensor.nnz))

        # train and evaluate each model on the current split
        for m, model in enumerate(models):
            # training model and measuring the time for training
            starttime = time.time()
            model.fit(X_train)
            traintime = time.time() - starttime
            traintimes[m] += traintime/float(len(splits))

            # predict triples
            y_pred = model.predict(test_idcs)
            message = [type(model).__name__, model.params]

            # evaluate results based on different metrics
            for f, func in enumerate(metrics):
                scores[f, m, i] = func(y_test, y_pred)
                message.append(str(func.__name__)+":")
                message.append(scores[f, m, i])

            message.append('Time:')
            message.append(traintime)
            if hasattr(model, 'used_epochs'):
                message.append('(Epochs:%d, Mean: %.3f (std=%.3f), Median: '
                               '%.3f)' % (model.used_epochs,
                                          np.mean(model.epoch_times),
                                          np.std(model.epoch_times),
                                          np.median(model.epoch_times)))
            _log.info(' '.join([str(m) for m in message]))

            # Needed, otherwise the parameters of all models blow the memory
            model.clear()

    # calculate score stats
    for i in xrange(scores.shape[0]):  # metrics
        for j in xrange(scores.shape[1]):  # models
            print scores[i, j, :]
            scores_stats[i, j, 0] = np.mean(scores[i, j, :])
            scores_stats[i, j, 1] = np.std(scores[i, j, :])

    return scores_stats, scores


def validate_on_triple_tensor(tensor, splits, models, workers=1,
                              metrics=[auprc, auroc], savepath="./tmp"):
    """
    Perform (cross)-validation on a dataset that has a (Coo/Lil)TripleTensor
    data structure. Parallelize model training and evaluation using joblib.
    Parameters
    ----------
    tensor : :class:`~TripleTenosr`
         The 3D (triple) tensor.
    models : list of :class:`~models`
         Latent Variable Models that learn based on indices.
    splits :  list of :class:`numpy.ndarray`
        nx3 numpy arrays where each array contains the indices, that are set to
        zero and evaluated against after training.
    workers : int
        Parallelize the validation with n worker, where each worker processes
        a single model.
    metrics : list of callable, optional
        Functions that compute an evaluation metric.
        Defaults to [auprc, auroc].
    savepath : str
        Temporary folder where the shared memmaps are saved.

    """
    # Check if the amount of workers is valid
    if workers > mp.cpu_count():
        workers = mp.cpu_count()
        warnings.warn('More workers than available were selected, setting'
                      'amount of workers to the maximum of %d!'
                      % mp.cpu_count())

    # Define shared variables between the workers
    jobid = int(round(time.time() * 1000))
    savepath = '%s_%d' % (savepath, jobid)
    if not os.path.isdir(savepath):
        os.mkdir(savepath)

    # the memmaps are overwriten by each worker in parallel
    scores = np.memmap(os.path.join(savepath, 'scores_%d.mmap' % jobid),
                       dtype=np.float32, mode='w+',
                       shape=(len(metrics), len(models), len(splits)))

    traintimes = np.memmap(os.path.join(savepath,
                                        'traintimes_%d.mmap' % jobid),
                           dtype=np.float32, mode='w+', shape=(len(models),))
    try:
        # Parallize each iteration of the cross-validatioin step to save time
        # when preparing the data
        for i, test_idcs in enumerate(splits):
            # prepare shared training and test data
            # binary prediction targets
            y_test = tensor[test_idcs]
            # training tensor
            X_train = tensor.copy()
            X_train[test_idcs] = 0
            _log.info('Size Testset: %d, NNZ Testset: %d, NNZ Trainset: %d, '
                      'NNZ Complete: %d' % (splits[i].shape[0],
                                            csr_matrix(y_test).nnz,
                                            X_train.nnz, tensor.nnz))

            # Define fold specific shared data between workers
            y_test_file = os.path.join(savepath, 'ytest_%d.mmap' % jobid)
            test_idcs_file = os.path.join(savepath,
                                          'test_idcs_%d.mmap' % jobid)

            # create numpy memmaps
            joblib.dump(y_test, y_test_file)
            joblib.dump(test_idcs, test_idcs_file)

            # reload as numpy memmaps
            test_idcs = joblib.load(test_idcs_file, mmap_mode='r')
            y_test = joblib.load(y_test_file, mmap_mode='r')

            joblib.Parallel(n_jobs=workers)(joblib.delayed(worker)(
                X_train, test_idcs, y_test, scores, traintimes, metrics, i,
                midx, model)
                for midx, model in enumerate(models))
    finally:
        try:
            shutil.rmtree(savepath)
        except:
            print("Failed to delete: " + savepath)

    # convert the shared numpy arrays
    scores = np.ctypeslib.as_array(scores)
    traintimes = np.ctypeslib.as_array(traintimes)/len(splits)

    # calculate score stats
    scores_stats = np.zeros((len(metrics), len(models), 2))  # mean and std
    for i in xrange(scores.shape[0]):  # metrics
        for j in xrange(scores.shape[1]):  # models
            scores_stats[i, j, 0] = np.mean(scores[i, j, :])
            scores_stats[i, j, 1] = np.std(scores[i, j, :])

    return scores_stats, scores


def worker(X_train, test_idcs, y_test, scores, traintimes, metrics, split_id,
           midx, model):
    """
    Function that is performed by each worker:
    Each worker trains one model and evaluates its performance
    """
    # training model and measuring the time for training
    starttime = time.time()
    model.parallization_precautions = True
    model.fit(X_train)
    traintime = time.time() - starttime
    traintimes[midx] += traintime

    # predict triples
    y_pred = model.predict(test_idcs)
    message = [type(model).__name__, model.params]

    # evaluate results based on different metrics
    for f, func in enumerate(metrics):
        scores[f, midx, split_id] = func(y_test, y_pred)
        message.append(str(func.__name__)+":")
        message.append(scores[f, midx, split_id])

    message.append('Time:')
    message.append(traintime)
    if hasattr(model, 'used_epochs'):
        message.append('(Epochs:%d, Mean: %.3f (std=%.3f), Median: %.3f)'
                       % (model.used_epochs, np.mean(model.epoch_times),
                          np.std(model.epoch_times),
                          np.median(model.epoch_times)))

    _log.info(" ".join([str(m) for m in message]))

    # Needed, otherwise the parameters of all models blow the memory
    model.clear()


def evaluate_experiment(score_stats, models, metrics, label_best=0,
                        label_size_limit=60):
    """
    Function to visualize results in ASCII format
    Parameters
    ----------
    score_stats : :class:`numpy.ndarray`
        2D numpy array where each row of the matrix holds the scores for a
        specific metric each column holds the model in the same order as in
        models.
    models : list
        models
    metrics : list of callable
        Evaluation metric functions
    """
    labels = [type(m).__name__+str(m.params) for m in models]
    for i in xrange(len(metrics)):
        best_idcs = get_best(score_stats[i, :, 0], models, label_best)
        print "Metric:", metrics[i].__name__
        bars_with_scores(labels, score_stats[i, :, 0], std=None, bar_size=25,
                         label_size_limit=label_size_limit,
                         label_best=best_idcs)
        print


def get_best(score_stats, models, labels_best):
    if labels_best is None:
        return np.array([np.argmax(score_stats)])
    else:
        cats = {}
        # get all categories
        rnd = 0

        for m, model in enumerate(models):
            if type(model).__name__ == 'RandomTensorModel':
                rnd = m
            else:
                cats[model.params[labels_best]] = 0

        for k in cats.keys():
            cats[k] = rnd
        for m, model in enumerate(models):
            if not type(model).__name__ == 'RandomTensorModel':
                c = model.params[labels_best]
                model_score = score_stats[m]
                current_best = cats[c]
                current_best_score = score_stats[current_best]
                if current_best_score < model_score:
                    cats[c] = m
        return np.array([cats[k] for k in cats.keys()])
