from .triple_tensor import CooTripleTensor
from .triple_tensor import LilTripleTensor
from .rescal2_tensor import Rescal2Tensor
