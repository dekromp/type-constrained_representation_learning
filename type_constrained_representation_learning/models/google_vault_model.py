import itertools
import numpy as np
import os
import theano as th
import theano.tensor as T
import theano.sandbox.rng_mrg as th_rng
from theano.tensor.nnet import binary_crossentropy
import time

from ..data_structures import triple_tensor as tt
from .gradient_descent import adagrad
from ..experiments.metrics import auprc
from ..experiments.helper import tolist
from .optimization import sgd_on_triples
DTYPE = th.config.floatX


def nn_dropout(X, rg, p=.5):
    """Sets random entries in a matrix to zero with probability p.

    Parameters
    ----------
    X : :class:`theano.tensor.TensorVariable`
        input tensor which is not sparse.
    rg : :class:`theano.sandbox.rng_mrg.RandomStream`
        Random number generator.
    p : float
        Probability that an entry is not set to zero.
        Defaults to 0.5.

    """
    if p == 1:
        return X
    else:
        return X * rg.binomial(size=X.shape, p=p, dtype=X.dtype)


def maxmargin(X, Xc):
    return T.maximum(0, 1 - X + Xc)


class GoogleVaultModel(object):
    """The Google Knowledge Vault Neural Network.

    Implementation of the Neural Network Model used in the Google Knowledge
    Vault Paper:
    Xin Luna Dong, Evgeniy Gabrilovich, Geremy Heitz, Wilko Horn, Ni Lao,
    Kevin Murphy, Thomas Strohmann, Shaohua Sun, Wei Zhang
    Knowledge Vault: A Web-Scale Approach to Probabilistic Knowledge Fusion
    Proceedings of the 20th ACM SIGKDD International Conference on Knowledge
    Discovery and Data Mining (2014)

    Parameters
    ----------
    seed : int
       The seed used for the numpy random number generator.
    seed_theano : int
       The seed used for the theano random number generator.
    K : int
        The length of the embeddings. 3K is the number of the nodes of the
        first layer.
    L : int
        Number of nodes in the second layer.
    A : 2D numpy array (optional)
        Initialization for weights of the first layer after embeddings (3K x L)
    E : 2D numpy array (optional)
        Initialization for subject embeddings (|Entities| x K)
    W : 2D numpy array (optional)
        Initialization for predicate embeddings (|Predicates| x K)
    beta : 2D numpy array (optional)
        Initialization for weights of second layer (1xL)
    cost : callable
        Loss function.
        Defaults to :classs:`theano.tensor.nnet.binary_crossentropy`.
    labeled_data : bool
        If True, cost is a function that evaluates the score based on labels
        (e.g. binary-cross-entropy).
        If False, cost is a function that evaluates the model based on
        unlabeled data through a ranking measure (e.g. max margin).
        Defaults to `True`.
    l1_reg : float
        L1 regularization strength.
        Defaults to 0.0.
    l2_reg : float
        L2 regularization strength.
        Defaults to 0.0.
    dropout : float
        [0, 1] strength of dropout regularization.
        Defaults to 0.0.
    learn_rate : float
        Learning rate.
        Defaults to 0.01.
    google_lcwa : bool
        If true, than the local closed-world assumption of the google paper is
        used.
        Defaults to `False`.
    lcwa : bool
        If true and google_lcwa == False, approximate the type constraints from
        the data by our proposed local closed-world assumption.
        Defaults to `False`.
    corrupted : int
        Number of corrupted entries used per sampled triple.
        Defaults to 1.
    corrupted_axes : list
        Axes  from which the corrupted is sampled
        ([0] = Subject is changed, [1] = Object is changed, can be combined
        ([0,1])).
        Defaults to [1].
    samplefunc : string
        `bordes` (negative sampling) or `default`.
        Defaults to `default`.
    mbatchsize : int
        Size of mini-batches.
        Defaults to 128.
    maxepoch : int
        Maximum of epochs for optimization.
        Defaults to 200.
    neval : int
        Validate performance every nth mini-batch.
        Defaults to 100.
    savepath : string
        Where to save the best model.

    """
    def __init__(self, seed, seed_theano, K, L, A=None, E=None, W=None,
                 beta=None, cost=binary_crossentropy,
                 labeled_data=True, l1_reg=0, l2_reg=0, dropout=0.0,
                 learn_rate=1e-2, corrupted=1, corrupted_axes=[1],
                 samplefunc='default', maxepoch=200, mbatchsize=128, neval=100,
                 google_lcwa=False, lcwa=False,
                 savepath='./google_vault_model', dtype=DTYPE,
                 normal_init=1e-2, conv=1e-4,
                 mid=np.random.randint(1000000)):

        model_id = (time.strftime('%d_%m_%y___%H_%M_%S') +
                    '_%d-%d_' % (mid, np.random.randint(1000000)))
        self.seed = seed
        self.rng = np.random.RandomState(seed)
        self.thrng = th_rng.MRG_RandomStreams(seed_theano)
        self.corrupted = corrupted
        self.corrupted_axes = corrupted_axes
        self.samplefunc = (tt.compute_corrupted_bordes
                           if samplefunc == "bordes" else tt.compute_corrupted)
        self.maxepoch = maxepoch
        self.mbatchsize = mbatchsize
        self.neval = neval
        self.labeled_data = labeled_data
        self.learn_rate = learn_rate
        self.K = K
        self.L = L
        self.A = A
        self.E = E
        self.W = W
        self.beta = beta
        self.cost = cost
        self.dropout = dropout
        self.l1_reg = l1_reg
        self.l2_reg = l2_reg
        self.normal_init = normal_init
        self.google_lcwa = google_lcwa
        self.lcwa = lcwa
        self.dtype = dtype
        self.savefile = os.path.join(savepath,
                                     model_id+type(self).__name__+".pkl")
        self.parallization_precautions = False
        self.conv = conv

        if self.google_lcwa:
            self.corrupted_axes = [1]

        # Create path where the trained model is saved
        if not os.path.isdir(savepath):
            os.mkdir(savepath)

        self.params = [self.K, self.L, self.l1_reg, self.l2_reg, self.dropout,
                       self.learn_rate, self.cost.__name__[0], self.corrupted,
                       self.normal_init, self.google_lcwa, self.lcwa, seed,
                       self.corrupted_axes]

    def loss_func(self, indices, Y):
        # Metric used for early stopping (area under precision recall curve)
        return 1-auprc(Y, self.nn_func(indices))

    def fit(self, tensor):
        if self.google_lcwa:
            # Remove Type constraints on the object axis if the original lcwa
            # is used.
            for i in xrange(len(tensor.type_constraints)):
                tensor.type_constraints[i][1] = None
        elif self.lcwa:
            tensor.approximate_type_constraints()

        # Initialization of Parameters
        X = T.imatrix("X")
        if self.E is None:  # Initialize the latent embeddings for the Subjects
            self.E = th.shared(value=np.array(
                self.rng.normal(0, self.normal_init,
                                (tensor.shape[0], self.K)),
                dtype=self.dtype), name="Ents_emb")
        # Initialize the latent embeddings for the Predicates.
        if self.W is None:
            self.W = th.shared(value=np.array(
                self.rng.normal(0, self.normal_init,
                                (tensor.shape[2], self.K)),
                dtype=self.dtype), name="Pred_emb")
        if self.A is None:  # Initialize the first layer
            self.A = th.shared(value=np.array(
                self.rng.normal(0, self.normal_init, (self.L, 3 * self.K)),
                dtype=self.dtype), name="A")
        if self.beta is None:  # Initialize the second layer
            self.beta = th.shared(value=np.array(
                self.rng.normal(0, self.normal_init, (1, self.L)),
                dtype=self.dtype), name="beta")

        self.parameters = [self.A, self.E, self.W, self.beta]
        # Model graph
        nn_output = self.__graph_pred(X)
        # Neural Network function
        self.nn_func = th.function([X], nn_output)

        if self.labeled_data:
            # Cost function
            Y = T.matrix("Y")
            loss = self.__graph_train_bc(X, Y).mean()
            # Update function
            self.update_func = adagrad([X, Y], loss, self.parameters,
                                       lr=self.learn_rate)
            # Train the model with stg
            fitted_parameters, self.used_epochs, self.epoch_times = (
                sgd_on_triples(self.rng, tensor, self, neval=self.neval,
                               mbsize=self.mbatchsize, unlabeled=False,
                               copy_X_train=not self.parallization_precautions)
                                                                     )
        else:
            # Cost function
            Xc = T.imatrix("Xc")  # corrupted
            loss = self.__graph_train_mm(X, Xc).mean()
            # Update function
            self.update_func = adagrad([X, Xc], loss, self.parameters,
                                       lr=self.learn_rate)
            # Train the model with stg
            fitted_parameters, self.used_epochs, self.epoch_times = (
                sgd_on_triples(self.rng, tensor, self, neval=self.neval,
                               mbsize=self.mbatchsize, unlabeled=True,
                               copy_X_train=not self.parallization_precautions)
                                                                     )

        # Update model parameters
        for i, parameter in enumerate(fitted_parameters):
            self.parameters[i].set_value(parameter.get_value())

    def __graph_pred(self, X):
        # No updated/shared variable is allowed to be indexed twice
        e = self.E[X[:, :2].T.reshape((-1,))]
        # Select latent embeddings by the input indices
        u = e[:e.shape[0]//2]
        w = self.W[X[:, 2]]
        v = e[e.shape[0]//2:]
        uwv = T.concatenate([u, w, v], axis=1)
        # Feed selected latent embeddings into the mlp
        nn_output = T.nnet.sigmoid(
            T.dot(self.beta, (1-self.dropout)*T.tanh(T.dot(self.A, uwv.T)))).T
        return nn_output

    def __graph_train_bc(self, X, Y):
        # Graph for binary cross entropy cost function
        # No updated/shared variable is allowed to be indexed twice
        e = self.E[X[:, :2].T.reshape((-1,))]
        # Select latent embeddings by the input indices
        u = e[:e.shape[0]//2]
        w = self.W[X[:, 2]]
        v = e[e.shape[0]//2:]
        uwv = T.concatenate([u, w, v], axis=1)
        # Elastic Net Regularization
        L1 = self.l1_reg * T.sum(T.abs_(uwv), axis=1)
        L2 = self.l2_reg * T.sqrt(T.sum(T.sqr(uwv), axis=1))
        L1 = L1.reshape((L1.shape[0], 1))
        L2 = L2.reshape((L2.shape[0], 1))
        # Feed selected latent embeddings into the mlp
        return (self.cost(T.nnet.sigmoid(
            T.dot(self.beta, nn_dropout(T.tanh(T.dot(self.A, uwv.T)),
                                        self.thrng, p=1-self.dropout))).T,
                          Y) + L1 + L2)

    def __graph_train_mm(self, X, Xc):
        # Graph for max-margin cost function
        # No updated/shared variable is allowed to be indexed twice!
        E = self.E[T.concatenate([X[:, :2], Xc[:, :2]],
                                 axis=1).T.reshape((-1,))]
        W = self.W[T.concatenate([X[:, 2], Xc[:, 2]])]

        def nnfunc(uwv):
            return (T.nnet.sigmoid(
                T.dot(self.beta, nn_dropout(T.tanh(T.dot(self.A, uwv.T)),
                                            self.thrng, p=1-self.dropout))).T)

        # Select latent embeddings for the true triples
        e = E[:E.shape[0]//2]
        u = e[:e.shape[0]//2]
        w = W[:W.shape[0]//2]
        v = e[e.shape[0]//2:]
        uwv = T.concatenate([u, w, v], axis=1)

        # Select latent embeddings for the corrupted
        ec = E[E.shape[0]//2:]
        uc = ec[:ec.shape[0]//2]
        wc = W[W.shape[0]//2:]
        vc = ec[ec.shape[0]//2:]
        uwvc = T.concatenate([uc, wc, vc], axis=1)

        L1 = self.l1_reg * T.sum(T.abs_(
                T.concatenate([uwv, uwvc], axis=1)), axis=1)
        L2 = self.l2_reg * T.sqrt(T.sum(T.sqr(
                T.concatenate([uwv, uwvc], axis=1)), axis=1))
        L1 = L1.reshape((L1.shape[0], 1))
        L2 = L2.reshape((L2.shape[0], 1))
        return self.cost(nnfunc(uwv), nnfunc(uwvc)) + L1 + L2

    def predict(self, indices):
        return self.nn_func(indices).flatten()

    def clear(self):
        del self.A
        del self.E
        del self.W
        del self.beta
        del self.parameters
        os.remove(self.savefile)

    @staticmethod
    def model_creator(settings):
        # For loading multiple model parameters from a configuration file
        confa = None
        if settings['try_all_reg_combinations']:
            confs = list(itertools.product(tolist(settings['rank']),
                                           tolist(settings['layer2_nodes']),
                                           tolist(settings['lambda_l1']),
                                           tolist(settings['lambda_l2']),
                                           tolist(settings['dropout']),
                                           tolist(settings['corrupted']),
                                           tolist(settings['learn_rate']),
                                           tolist(settings['normal_init'])))
        else:
            confs = [[r, l, l1, l2, d, c, lr, init]
                     for r, l, l1, l2, d, c, lr, init in
                     zip(tolist(settings['rank']),
                         tolist(settings['layer2_nodes']),
                         tolist(settings['lambda_l1']),
                         tolist(settings['lambda_l2']),
                         tolist(settings['dropout']),
                         tolist(settings['corrupted']),
                         tolist(settings['learn_rate']),
                         tolist(settings['normal_init']))]

        confs = list(itertools.product(tolist(settings['seed']), confs))
        models = []

        for i, conf in enumerate(confs):
            s, conf = conf
            r, l, l1, l2, d, c, lr, init = conf
            rng = np.random.RandomState(s)
            seed_theano = rng.randint(1000000)
            models.append(
                GoogleVaultModel(s, seed_theano, r, l,
                                 A=None, E=None, W=None, beta=None,
                                 cost=eval(settings['cost']),
                                 labeled_data=settings['is_labeled_data'],
                                 l1_reg=l1, l2_reg=l2, dropout=d,
                                 learn_rate=lr,
                                 corrupted=c,
                                 corrupted_axes=(
                                    tolist(settings['corrupted_axes'])),
                                 samplefunc=settings["samplefunc"],
                                 maxepoch=settings['maxepoch'],
                                 mbatchsize=settings['mbatchsize'],
                                 neval=settings['neval'],
                                 normal_init=init,
                                 google_lcwa=settings['google_lcwa'],
                                 lcwa=settings['lcwa'],
                                 savepath=settings['savepath'],
                                 dtype=DTYPE,
                                 mid=i))

        return models
