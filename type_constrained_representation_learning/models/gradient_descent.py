import theano as th
import theano.tensor as T
import numpy as np


def gd(inputs, cost, parameters, lr=1e-2):
    """
    Parameters
    ----------
    inputs : list of Theano variables
    cost : Theano scalar
    parameters : list of Theano shared variables
    lr : Python float or numpy scalar float array
    """

    # compute gradients
    grads = __grad(cost, parameters)

    # added this, since TransE has different learning rates for the differen
    # matrices
    lrs = ([lr for i in xrange(len(grads))]
           if not isinstance(lr, list) and not isinstance(lr, np.ndarray)
           else lr)

    # compute updates
    updates = []
    for p, g, lr in zip(parameters, grads, lrs):
        if isinstance(g, tuple):
            # the gradient has non-zero components, but they are omitted so by
            # using the above `grad` function, we have a 2-tuple of the
            # non-zero gradient and the corresponding index variable
            g, i = g
            d = -lr * g
            # the updates must only be applied to the indexed components
            p_update = T.inc_subtensor(p[i], d)
        else:
            # this is the standard update rule
            d = -lr * g
            # standard update
            p_update = p + d

        updates.append((p, p_update))

    return th.function(inputs, cost, updates=updates)


def adagrad(inputs, cost, parameters, lr=1e-2):
    """
    Parameters
    ----------
    inputs : list of Theano variables
    cost : Theano scalar
    parameters : list of Theano shared variables
    lr : Python float or numpy scalar float array
    """

    # compute gradients
    grads = th.grad(cost, parameters)

    # added this, since TransE has different learning rates for the differen
    # matrices
    lrs = ([lr for i in xrange(len(grads))]
           if not isinstance(lr, list) and not isinstance(lr, np.ndarray)
           else lr)

    # memory of squared gradients
    squared_grads_memory = [th.shared(np.zeros_like(p.get_value(borrow=True)),
                                      borrow=True)
                            for p in parameters]

    # compute AdaGrad updates
    updates = []
    for p, g, sgm, lr in zip(parameters, grads, squared_grads_memory, lrs):
        if isinstance(g, tuple):
            raise NotImplementedError("Faster computing of updates has errors")
            # the gradient has non-zero components, but they are omitted so by
            # using the above `grad` function, we have a 2-tuple of the
            # non-zero gradient and the corresponding index variable
            g, i = g

            # add current squared gradient to the so far accumulate squared
            # gradients
            # tmp = T.inc_subtensor(tmp[i],g) 
            # tmp = sparse.csr_from_dense(tmp)
            # sg = (sgm + sparse.sqr(tmp))[i]
            # tmp = T.inc_subtensor(tmp[i],-g)
            sg = sgm[i] + T.sqr(g)

            # take the square root of the sum of all squared gradients
            sg_sqrt = T.sqrt(sg + 1e-10)

            # the update step is the negative gradient divided elementwise by
            # the square root of the sum of all squared gradients
            d = -lr * (g / sg_sqrt)

            # the updates must only be applied to the indexed components
            p_update = T.inc_subtensor(p[i], d)
            sgm_update = T.set_subtensor(sgm[i], sg)
        else:
            # this is the standard update rule

            # add current squared gradient to the so far accumulate squared
            # gradients
            sg = sgm + T.sqr(g)

            # take the square root of the sum of all squared gradients
            sg_sqrt = T.sqrt(sg + 1e-10)

            # the update step is the negative gradient divided elementwise by
            # the square root of the sum of all squared gradients
            d = -lr * (g / sg_sqrt)

            # standard update
            p_update = p + d
            sgm_update = sg

        updates.append((p, p_update))
        updates.append((sgm, sgm_update))

    return th.function(inputs, cost, updates=updates)


def __grad(*args, **kwargs):
    """
    Computes the gradient as usual, but detects gradients of parameters that
    were indexed along the first dimension because all non-indexed components
    are zero in the gradient. In this case, only the non-zero gradient
    components are returned along with the corresponding index variable.

    Parameters
    ----------
    see theano.grad function

    Returns
    -------
    gradient(s) or tuple(s) of gradient and index variable
    """
    grads = []
    for g in th.grad(*args, **kwargs):
        if isinstance(g.owner.op, T.AdvancedIncSubtensor1):
            grads.append(tuple(g.owner.inputs[1:]))
        else:
            grads.append(g)
    return grads
