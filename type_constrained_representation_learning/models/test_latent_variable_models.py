import unittest, sys, os, logging 
import numpy as np
sys.path.append('../../')
import type_constrained_representation_learning.data_structures\
    .triple_tensor as tt
from type_constrained_representation_learning.experiments.metrics import auroc
from type_constrained_representation_learning.experiments.metrics import auprc
from type_constrained_representation_learning.models import (
    TranslationalEmbeddingsModel as transE)
from type_constrained_representation_learning.models import (
    GoogleVaultModel as gvm)
from type_constrained_representation_learning.models import Rescal2
from type_constrained_representation_learning.models import RandomTensorModel

from type_constrained_representation_learning.experiments\
    .experiment_settings_parser import ExperimentSettingsParser
from type_constrained_representation_learning.experiments.helper import (
    delete_files)
import logging
logging.basicConfig(level=logging.INFO)

#logging.basicConfig(level=logging.DEBUG)
class Test(unittest.TestCase):

    def setUp(self):
        n,m,k = 7,7,6
        self.test_tensors = []  
        self.seed = 1407
        self.test_tensors.append(tt.random_splist_without_tc(n,m,k,self.seed,func=tt.random_cp_generated_splist))
        self.test_tensors.append(tt.random_splist_with_approx_tc(n,m,k,self.seed,func=tt.random_cp_generated_splist))
        self.test_tensors.append(tt.random_splist_with_tc_mix(n,m,k,self.seed,func=tt.random_cp_generated_splist))
        self.eparser = ExperimentSettingsParser("./test_model_settings.ini")
        self.random_model = RandomTensorModel()
        
    def testTransEModel(self):
        print "Testing TranslationalEmbeddingsModel..."
        settings = self.eparser.get_settings("TranslationalEmbeddingsModel", "Test")
        self.__routine_for_model_testing(settings,transE.model_creator)
        print
        
    def testTransEModelMyLCA(self):
        print "Testing TranslationalEmbeddingsModel with my LCWA..."
        settings = self.eparser.get_settings("TranslationalEmbeddingsModel", "Test_lcwa")
        self.__routine_for_model_testing(settings,transE.model_creator)
        print
        
    def testTransEModelNoTc(self):
        print "Testing TranslationalEmbeddingsModel without considering type constraints..."
        settings = self.eparser.get_settings("TranslationalEmbeddingsModel", "Test_notc")
        self.__routine_for_model_testing(settings,transE.model_creator)
        print
    
    
    def testGoogleVaultModelLabeled(self):
        print "Testing GoogleVaultModel with labeled data..."
        settings = self.eparser.get_settings("GoogleVaultModel", "Test_labeled")
        self.__routine_for_model_testing(settings,gvm.model_creator)
        print 
    
    def testGoogleVaultModelLabeledMyLCA(self):
        print "Testing GoogleVaultModel with labeled data and my lcwa..."
        settings = self.eparser.get_settings("GoogleVaultModel", "Test_labeled_lcwa")
        self.__routine_for_model_testing(settings,gvm.model_creator)
        print 
    
    def testGoogleVaultModelLabeledGoogleLCA(self):
        print "Testing GoogleVaultModel with labeled data and google lcwa..."
        settings = self.eparser.get_settings("GoogleVaultModel", "Test_labeled_glcwa")
        self.__routine_for_model_testing(settings,gvm.model_creator)
        print 
        
    def testGoogleVaultModelUnlabeled(self):  
        print "Testing GoogleVaultModel with unlabeled data..." 
        settings = self.eparser.get_settings("GoogleVaultModel", "Test_unlabeled")
        self.__routine_for_model_testing(settings,gvm.model_creator)
        print
        
    def testGoogleVaultModelBordesSampling(self):  
        print "Testing GoogleVaultModel with bordes sampling..." 
        settings = self.eparser.get_settings("GoogleVaultModel", "Test_bordes_sampling")
        self.__routine_for_model_testing(settings,gvm.model_creator)
        print
        
    def testRescal2(self):
        print "Testing Rescal2..." 
        settings = self.eparser.get_settings("Rescal2", "Test")
        self.__routine_for_model_testing(settings,Rescal2.model_creator)
        print
        
    def testRescal2TC(self):
        print "Testing Rescal2_tc..." 
        settings = self.eparser.get_settings("Rescal2", "Test_tc")
        self.__routine_for_model_testing(settings,Rescal2.model_creator)
        print
        
    def testRescal2LCA(self):
        print "Testing Rescal2_lca..." 
        settings = self.eparser.get_settings("Rescal2", "Test_lcwa")
        self.__routine_for_model_testing(settings,Rescal2.model_creator)
        print
        
    def __routine_for_model_testing(self,settings,func,threshold=0.55):
            
        for z,ti in enumerate(self.test_tensors):
            model = func(settings)
            #test model creator output
            self.failUnless(len(model)==1)
        
            threshold = 0.55
            
            model = model[0]
            #test_sample
            test_sample=tt.sample_triples_with_nzeros(ti, 3, 3, [0,1], 
                                              remove_duplicates=True, 
                                              rng=np.random.RandomState(self.seed), 
                                              allow_corrupted_ones=True, 
                                              as_pairs=False,with_labels=False)
            
            y = ti[test_sample]
            threshold = auprc(y, self.random_model.predict(test_sample))
            print "Test Tensor %d: Score of Model is better than auprc %f, => "%(z,threshold),
            
            #test fit function
            model.fit(ti)
            y_hat = model.predict(test_sample)
            score_model = auprc(y, y_hat)

            print score_model, threshold,
            self.failUnless(threshold < score_model,"Model Score is below %f: %f"%(threshold,score_model))
            print "OK"
            model.clear()
            if settings.has_key('savepath'):
                delete_files(settings['savepath'])


if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testGoogleVaultModelLabeled']
    unittest.main()