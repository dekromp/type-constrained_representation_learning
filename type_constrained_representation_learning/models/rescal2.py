import itertools
import logging
import numpy as np
from numpy import dot
from numpy import zeros
from numpy import array
from numpy import eye
from numpy import kron
from numpy.random import rand
from numpy.linalg import inv
from numpy.linalg import svd
import os
from scipy.sparse import csr_matrix
from scipy.sparse import coo_matrix
from scipy.sparse import hstack
from scipy.sparse.linalg import eigsh
import time

from ..data_structures import Rescal2Tensor
from ..data_structures import LilTripleTensor
from ..experiments.metrics import auprc
from ..experiments.helper import tolist
from .optimization import als_on_triples

_log = logging.getLogger(__name__)


class Rescal2(object):
    """Modified RESCAL which supports type-constraints.

    The RESCAL Tensor Factorization with/without Type-Constraints and early
    stopping based on validation set.

    Parameters
    ----------
    rank: int
       Rank of the factorization (Length of embeddings).
    lmbda_A : float
        L2 regularization strength on the factor matrix A.
    lmbda_R : float
        L2 regularization strength on the core tensor R.
    consider_tc : boolean
        Whether or not to consider type constraints
    lcwa : bool
        If True, all type_constraints are approximated
        otherwise type constraints are only approximated if absent (using the
        local closed world assumption).
        Defaults to False.
    maxepoch : int
        Number of iterations and updates performed.
        Defaults to 50.
    init : 'nvecs' or 'random'
        Initialization used for factor matrix A.
        Defaults to 'nvecs'.
    init_value : float
        Std for numpy.random.normal for random initialization.
        Defaults to 0.05.
    seed : int
        Seed used for sampling the validation set.
        Defaults to 1407.
    neval : int
        Check validation error after n epochs (1=every epoch).
        Defaults to 1.
    conv : float
        Convergence criteria.
        Defaults to 0.0001.
    savepath : string
        Folder where to save the best temporary best models.
        Defaults to '/tmp'.

    """
    def __init__(self,  rank, lmbda_A, lmbda_R,
                 consider_tc=False,
                 lcwa=False,
                 maxepoch=50,
                 init='nvecs',
                 init_value=0.05,
                 seed=1407,
                 conv=1e-4,
                 neval=1,
                 savepath='./tmp',
                 early_stopping=True,
                 mid=np.random.randint(1000000)):

        model_id = (time.strftime("%d_%m_%y___%H_%M_%S") +
                    "_%d-%d_" % (mid, np.random.randint(1000000)))
        self.rank = rank
        self.lmbda_A = lmbda_A
        self.lmbda_R = lmbda_R
        self.seed = seed
        self.maxepoch = maxepoch
        self.init = init
        self.init_value = init_value
        self.consider_tc = consider_tc
        self.lcwa = lcwa
        self.parallelization_precautions = False
        self.dtype = np.float32
        self.conv = conv
        self.neval = neval
        self.early_stopping = early_stopping
        self.params = [self.rank, self.lmbda_A, self.lmbda_R, self.seed,
                       self.consider_tc, self.lcwa]
        self.savefile = os.path.join(savepath,
                                     model_id+type(self).__name__+".pkl")
        if not os.path.isdir(savepath):
            os.mkdir(savepath)

    def fit(self, X):
        if isinstance(X, LilTripleTensor):
            X = X.tocoo()

        self.parameters = []
        fitted_parameters, self.used_epochs, self.epoch_times = (
            als_on_triples(np.random.RandomState(self.seed), X, self,
                           neval=self.neval,
                           copy_X_train=not self.parallelization_precautions))
        for i, parameter in enumerate(fitted_parameters):
            self.parameters[i] = parameter

    def predict(self, indices):
        return self.factorization[indices]

    def clear(self):
        del self.factorization
        os.remove(self.savefile)

    def loss_func(self, indices, Y):
        # Error-Function used for early stopping
        # auprc score
        return 1.-auprc(Y, self.factorization[indices])

    def _initialize(self, X):
        # Initialization of Factor Matrices A and R
        _log.debug('Initializing A')
        if self.init == 'random':
            A = array(rand(X.shape[0], self.rank), dtype=self.dtype)
        elif self.init == 'nvecs':
            S = csr_matrix((X.shape[0], X.shape[0]), dtype=self.dtype)
            for i in xrange(X.shape[2]):
                TMP = X[i].tocsr()
                S = S + TMP
                S = S + TMP.T
            _, A = eigsh(csr_matrix(S, dtype=self.dtype, shape=(X.shape[0],
                                                                X.shape[0])),
                         self.rank)
            A = array(A, dtype=self.dtype)
        else:
            raise 'Unknown init option ("%s")' % self.init

        if self.consider_tc:
            # Initialize grouping of entities and constrained representation of
            # the tensor
            _log.debug('Initializing matrices needed for type-constraints and'
                       ' grouping of entities')
            self.S, self.O = self.__buildSO(X, self.lcwa)
            self.s, self.o, self.groups = self.__autoGrouping(X, self.S,
                                                              self.O)
            X = self.__shrinkX(X, self.S, self.O)

        R = (self.__updateR(X, A)
             if not self.consider_tc
             else self.__updateRtc(X, A, self.S, self.O))
        self.factorization = Rescal2Tensor(A, R)
        self.parameters = [self.factorization]
        return X

    def update(self, X):
        if self.consider_tc:
            # RESCAL2_TC ALS Updates:
            self.factorization.A = (
                self.__updateAtc(X, self.factorization.A, self.factorization.R,
                                 self.S, self.O, self.s, self.o, self.groups))
            self.factorization.R = (
                self.__updateRtc(X, self.factorization.A, self.S, self.O))
        else:
            # RESCAL2 ALS Updates
            self.factorization.A = (
                self.__updateA(X, self.factorization.A, self.factorization.R))
            self.factorization.R = self.__updateR(X, self.factorization.A)

    def __updateA(self, X, A, R):
        # Update step for A
        _log.debug('Updating A')
        n = X.shape[0]
        F = zeros((n, self.rank), dtype=self.dtype)
        E = zeros((self.rank, self.rank), dtype=self.dtype)
        AtA = dot(A.T, A)

        for i in range(X.shape[2]):
            Xi = X[i].tocsr()
            F += Xi.dot(dot(A, R[i].T)) + Xi.T.dot(dot(A, R[i]))
            E += dot(R[i], dot(AtA, R[i].T)) + dot(R[i].T, dot(AtA, R[i]))

        # regularization
        I = self.lmbda_A * eye(self.rank, dtype=A.dtype)

        # finally compute update for A
        A = dot(F, inv(I + E))
        return A

    def __updateR(self, X, A):
        # Update Step for R
        U, S, Vt = svd(A, full_matrices=False)
        Shat = kron(S, S)
        Shat = (Shat / (Shat ** 2 + self.lmbda_R)).reshape(self.rank,
                                                           self.rank)
        R = []
        for i in xrange(X.shape[2]):
            Rn = Shat * dot(U.T, X[i].tocsr().dot(U))
            Rn = dot(Vt.T, dot(Rn, Vt))
            R.append(Rn)
        return R

    def __updateAtc(self, X, A, R, S, O, s, o, groups):
        # Update Step for R
        n, rank = A.shape
        F = zeros((n, rank), dtype=A.dtype)
        E = zeros((rank, rank), dtype=A.dtype)
        ZS = np.zeros((len(X), rank, rank), dtype=A.dtype)
        ZO = np.zeros((len(X), rank, rank), dtype=A.dtype)

        # Precompute the first part of the update and the matrix products in
        # the inverse
        for i in range(len(X)):
            SAi = A[S[i].col]
            OAi = A[O[i].col]

            F[O[i].col] += dot(X[i].T * SAi, R[i])
            F[S[i].col] += dot(X[i] * OAi, R[i].T)

            ZS[i, :, :] = R[i].dot(OAi.T.dot(OAi)).dot(R[i].T)
            ZO[i, :, :] = R[i].T.dot(SAi.T.dot(SAi)).dot(R[i])

        # regularization
        I = self.lmbda_A * eye(rank, dtype=A.dtype)

        # Use the grouping to calculate the correct sums for the inverse
        for group_key in groups.keys():
            group = groups[group_key]
            gidx = group[0]
            if gidx not in s and gidx not in o:
                next
            elif gidx not in s:
                zo = np.array(ZO[o[gidx]])
                E = np.sum(zo, axis=0)
            elif gidx not in o:
                zs = np.array(ZS[s[gidx]])
                E = np.sum(zs, axis=0)

            else:
                zo = np.array(ZO[o[gidx]])
                zs = np.array(ZS[s[gidx]])
                E = np.sum(zo, axis=0)+np.sum(zs, axis=0)

            # Update A for all entities from the same group at once
            A[group, :] = F[group, :].dot(inv(I+E))
        return A

    def __updateRtc(self, X, A, S, O):
        # Update Step for R
        R = np.zeros((len(X), A.shape[1], A.shape[1]), dtype=A.dtype)
        for i in xrange(len(X)):
            U, D, Vt = svd(A[S[i].col], full_matrices=False)
            L, Q, Pt = svd(A[O[i].col], full_matrices=False)

            Shat = kron(Q, D)
            Shat = (Shat / (Shat ** 2 + self.lmbda_R)).reshape(len(Q),
                                                               len(D)).T

            R[i] = dot(Vt.T, dot(Shat * dot(U.T, X[i].dot(L)), Pt))
        return R

    def __buildSO(self, X, approx_all):
        S = []
        O = []
        for i in xrange(X.shape[2]):
            # Build Si
            if not X.type_constraints[i][0] is None and not approx_all:
                n = len(X.type_constraints[i][0])
                col = np.unique(X.type_constraints[i][0])  # needs to be sorted
            elif approx_all:
                n = len(np.unique(X[i].row))
                col = np.unique(X[i].row)
            else:  # include all indices
                n = X.shape[0]  # len(np.unique(X[i].row))
                col = np.arange(X.shape[0]).astype(np.int32)

            m = X[i].shape[0]
            row = np.arange(len(col))
            data = np.ones((n,))
            Si = csr_matrix((data, (row, col)), shape=(len(row), m))
            S.append((Si).tocoo())

            # Build Oi
            m = X[i].shape[1]
            if not X.type_constraints[i][1] is None and not approx_all:
                n = len(X.type_constraints[i][1])
                col = np.unique(X.type_constraints[i][1])  # needs to be sorted
            elif approx_all:
                n = len(np.unique(X[i].col))
                col = np.unique(X[i].col)
            else:  # include all indices
                n = X.shape[1]  # len(np.unique(X[i].col))
                col = np.arange(X.shape[1]).astype(np.int32)
            row = np.arange(len(col))
            data = np.ones((n,))
            Oi = csr_matrix((data, (row, col)), shape=(len(row), m))
            O.append((Oi).tocoo())

        return S, O

    def __autoGrouping(self, X, S, O):
        # Build type constraint matrices if not provided.
        # Groups entities that always occur together in a relation

        # important for grouping later on
        si_r = np.array([])
        si_c = np.array([])
        oi_r = np.array([])
        oi_c = np.array([])

        for i in xrange(X.shape[2]):

            si_r = np.append(si_r, S[i].col)
            si_c = np.append(si_c, np.ones((len(S[i].row),),
                                           dtype=np.int32) * i)

            oi_r = np.append(oi_r, O[i].col)
            oi_c = np.append(oi_c, np.ones((len(O[i].row),),
                                           dtype=np.int32) * i)

        si_d = np.ones((len(si_r),))
        oi_d = np.ones((len(oi_r),))

        # matrix: entities x relations, indicating in which relation an entity
        # e is valid as subject or object
        s = coo_matrix((si_d, (si_r, si_c)), shape=(X.shape[0], X.shape[2]))
        o = coo_matrix((oi_d, (oi_r, oi_c)), shape=(X.shape[0], X.shape[2]))
        G = hstack([s, o])

        # equal rows in matrix G => same group
        G = self.__buildRowsHash(G)
        groups = self.__groupWithStrings(G)

        s = self.__buildRowsHash(s)
        o = self.__buildRowsHash(o)

        return s, o, groups

    ########################################################
    # AUXILIARY FUNCTIONS #
    ########################################################
    def __groupWithStrings(self, rhash):
        # Uses string representation of column indices for fast group
        # assignment
        groups = {}
        z = 0
        for k in rhash.keys():
            key = str(rhash[k])
            if key in groups:
                groups[key].append(k)
            else:
                groups[key] = [k]
        return groups

    def __buildRowsHash(self, mat):
        # constructs a row_idx => col_indices hash for a binary matrix
        mlabels = mat.tocsr().tocoo()
        rows = {}
        for i in xrange(len(mlabels.data)):
            ridx = mlabels.row[i]
            cidx = mlabels.col[i]
            if ridx in rows:
                rows[ridx].append(cidx)
            else:
                rows[ridx] = [cidx]
        return rows

    def __shrinkX(self, X, S, O):
        # apply type constraints to the data
        Xs = range(X.shape[2])
        for k in xrange(X.shape[2]):
            Xs[k] = S[k] * X[k] * O[k].T

        return Xs

    @staticmethod
    def model_creator(settings):
        # For loading multiple model parameters from a configuration file
        confs = None
        if settings['try_all_reg_combinations']:
            confs = list(itertools.product(tolist(settings['rank']),
                                           tolist(settings['lambda_a']),
                                           tolist(settings['lambda_r']),
                                           tolist(settings['init_value'])))
        else:
            confs = [[r, la, lr, iv]
                     for r, la, lr, iv in
                     zip(tolist(settings['rank']),
                         tolist(settings['lambda_a']),
                         tolist(settings['lambda_r']),
                         tolist(settings['init_value']))]

        confs = list(itertools.product(tolist(settings['seed']), confs))
        models = []

        for i, conf in enumerate(confs):
            s, conf = conf
            r, la, lr, iv = conf
            models.append(Rescal2(r, la, lr,
                                  consider_tc=settings['consider_tc'],
                                  lcwa=settings['lcwa'],
                                  maxepoch=settings['maxepoch'],
                                  init='nvecs',
                                  seed=s,
                                  savepath=settings['savepath'],
                                  neval=settings['neval'],
                                  init_value=iv,
                                  early_stopping=settings['early_stopping'],
                                  mid=i))

        return models
