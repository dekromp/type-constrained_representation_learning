import numpy as np
import pickle
import logging
import time
from ..data_structures import triple_tensor as tt
_log = logging.getLogger(__name__)

IMPROVEMENT = 1e-4


class EarlyStopping(object):
    """
    Container that handles the early stopping and saving of the best
    parameters setting
    """
    def __init__(self, model, X_val, Y_val, tolerance=10):
        """
        Parameters:
        -----------
        model : LatentVariableModel
        X_val : 2D numpy array (np.int)
            First input for model functions
        Y_val : 2D numpy array (np.int)
            Second input for model functions
        tolerance : int
            The amount of times the validation error can be worse in a row
            than the best error
        """
        self.tolerance_default = tolerance
        self.tolerance = 0
        self.model = model
        self.best_error = np.inf
        self.X_val = X_val
        self.Y_val = Y_val
        self.file = model.savefile

    def check(self, global_mbatch_counter, mbatchsize):
        valerror = self.model.loss_func(self.X_val, self.Y_val)
        _log.debug("MiniBatch: %d;  Val Error: %f; %d total updates"
                   % (global_mbatch_counter, valerror,
                      global_mbatch_counter * mbatchsize))

        # check if valerror is better than the best
        # stop training imidiately if validation error is nan
        if np.isnan(valerror):
            _log.debug('Training was stopped, since the validation error is '
                       'nan!')
            return True

        if valerror+self.model.conv < self.best_error:
            self.best_error = valerror
            self.tolerance = self.tolerance_default
            self.save_best_model_parameters()
            # decrease the tolerance if validation error gets worse
        elif valerror >= self.best_error:
            self.tolerance -= 1
            if self.tolerance < 0:  # Stop Training and load best setting
                _log.debug('Training was stopped, since the validation error '
                           'is frequently getting worse!')
                return True
        return False

    def load_best_model_parameters(self):
        return pickle.load(open(self.file, 'rb'))

    def save_best_model_parameters(self):
        pickle.dump(self.model.parameters, open(self.file, 'wb'))


def __convert_idx(X, indices, as_pairs):
    if as_pairs:
        return indices[:, :3], indices[:, 3:]  # corrupted
    else:
        Y_val = indices[:, 3]
        Y_val = np.reshape(Y_val, (len(Y_val), 1)).astype(X.dtype)
        return indices[:, :3], Y_val


def sgd_on_triples(rng, X_train, model, neval=100, mbsize=128, unlabeled=True,
                   copy_X_train=False):
    """
    stochastic mini batch optimization based on triple data stored in a
    TripleTensor. To each triple, corrupted are computed

    Parameters
    ----------
    rng : numpy.random.RandomState
    X_train : TripleTensor
    model : LatentVariableModel
    neval : int
        compute fit on validation set after n mini batches
    mbsize : int
        size of minibatch
    unlabeled : Boolean
        True if a cost function is used which compares triples with corrupted
        ones (e.g. Bayesian Personalized Ranking, Max Margin)
        False if a cost function is used which evaluates the value of the
        predicted triple with the one in the data (e.g. binary crossentropy)

    """
    # build validation set and remove the ones from the tensor
    valset_size = int(X_train.nnz*0.05)
    X_val = tt.sample_triples_like_bordes(X_train, valset_size,
                                          zeros_per_triple=5,
                                          remove_duplicates=True, rng=rng,
                                          as_pairs=False, with_labels=True)

    # bring validation set in the right format
    X_val, Y_val = __convert_idx(X_train, X_val, unlabeled)

    # remove validation triples
    if copy_X_train:
        X_train = X_train.copy()
    X_train[X_val] = 0
    early_stopping = EarlyStopping(model, X_val, Y_val, tolerance=10)
    early_stopping.save_best_model_parameters()

    # MINI BATCH STOCHASTIC GRADIENT DESCENT ON TRIPLES
    global_mbatch_counter = 0
    epoch_times = []
    for i in xrange(model.maxepoch):
        etime = time.time()
        shuffled_triples = tt.sample_triples(X_train, X_train.nnz, rng)
        shuffled_triples = (
            shuffled_triples[rng.permutation(np.arange(X_train.nnz))])
        for bidx in xrange(X_train.nnz/mbsize):
            # get mini batch and compute corrupted
            mini_batch = shuffled_triples[bidx*mbsize:(bidx+1)*mbsize, :]
            mini_batch = model.samplefunc(
                X_train, mini_batch, model.corrupted, model.corrupted_axes,
                allow_corrupted_ones=isinstance(X_train, tt.CooTripleTensor),
                as_pairs=unlabeled, rng=rng)

            # bring mini batch in the right format
            mini_batch, Y_train = __convert_idx(X_train, mini_batch, unlabeled)
            # update the model parameters
            model.update_func(mini_batch, Y_train)
            # Translational EmbeddingsModel normalizes the Embeddings for the
            # Entities to L2 = 1
            if hasattr(model, "normalize"):
                batch_idcs = np.unique(mini_batch[:, :2].flatten())
                if Y_train.shape[1] == 3:
                    batch_idcs = np.unique(np.append(Y_train[:, :2].flatten(),
                                                     batch_idcs))
                model.normalize(batch_idcs)
                # model.normalize()
            # check validation error after neval mini batches
            global_mbatch_counter += 1
            if global_mbatch_counter % neval == 0:
                if early_stopping.check(global_mbatch_counter, mbsize):
                    return (early_stopping.load_best_model_parameters(),
                            i, epoch_times)

        # Measure time for each epoch
        t = time.time()-etime
        _log.debug("Epoch %d: %f sec" % (i+1, t))
        epoch_times.append(t)
    else:
        early_stopping.check(global_mbatch_counter, mbsize)

    return early_stopping.load_best_model_parameters(), i, epoch_times


def als_on_triples(rng, X_train, model, neval=100, copy_X_train=False):
    """
    alternating least-squares optimization based on triple data stored in a
    TripleTensor.

    Parameters:
    -----------
    rng : numpy.random.RandomState
    X_train : TripleTensor
        Training Data
    model : LatentVariableModel
    neval : int
        compute fit on validation set after n mini batches.

    """
    # build validation set and remove the ones from the tensor
    valset_size = int(X_train.nnz*0.05)
    X_val = tt.sample_triples_with_nzeros(
        X_train, valset_size, 10, [0, 1], remove_duplicates=True, rng=rng,
        allow_corrupted_ones=isinstance(X_train, tt.CooTripleTensor),
        as_pairs=False)

    # bring validation set in the right format
    X_val, Y_val = __convert_idx(X_train, X_val, False)

    # remove validation triples
    if copy_X_train:
        X_train = X_train.copy()
    X_train[X_val] = 0
    # initialization might be dependent on the data
    X_train = model._initialize(X_train)

    # save first model
    early_stopping = EarlyStopping(model, X_val, Y_val, tolerance=5)
    early_stopping.save_best_model_parameters()

    # Alternating Least-Squares on triples
    epoch_times = []
    for i in xrange(model.maxepoch):
        etime = time.time()
        model.update(X_train)
        if i % neval == 0:
            if model.early_stopping and early_stopping.check(i, 1):
                return (early_stopping.load_best_model_parameters(), i,
                        epoch_times)

        # Measure time
        t = time.time()-etime
        _log.debug("Epoch %d: %f sec" % (i+1, t))
        epoch_times.append(t)
    else:
        early_stopping.check(i, 1)

    return (early_stopping.load_best_model_parameters()
            if model.early_stopping else model.parameters, i, epoch_times)
