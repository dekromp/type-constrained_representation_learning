import numpy as np


class RandomTensorModel(object):

    def __init__(self,  seed=1407):
        self.seed = seed
        self.params = []
        self.parallization_precautions = False

    def reset(self):
        pass

    def fit(self, X):
        pass

    def predict(self, indices):
        rng = np.random.RandomState(self.seed)
        return rng.rand(len(indices))

    def clear(self):
        pass
