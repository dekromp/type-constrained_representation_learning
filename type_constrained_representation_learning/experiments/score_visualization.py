import numpy as np


def bars_with_scores(labels, scores, std=None, bar_size=20,
                     label_size_limit=20, label_best=None):
    """Simple ASCII based visualization of experimental results.

    model_name 0.964 ||||||||||||||||
    model_name 0.453 ||||||||
    model_name 0.875 ||||||||||||||

    Parameters
    ----------
    labels : list of strings
        Model names or other labels.
    scores : list of floats
        Scores of model or label.
    bar_size : int
        Maximum amount of bars used for visualization.
    label_size_limit : int
        Maximum length of label.

    """
    step = 1.0 / float(bar_size)
    L = np.empty((len(labels),), dtype=('a%d, a5, a%d' % (label_size_limit,
                                                          bar_size)))
    for l, label in enumerate(labels):
        label = shorten_label(label, label_size_limit)
        if len(label) >= label_size_limit:
            label = label[:label_size_limit]
        else:
            label = label + (label_size_limit-len(label)) * ' '

        score = '%.3f' % scores[l]
        bars = int(scores[l]/step)*"|"

        if std is None:
            if label_best is not None and l in label_best:
                print '%s %s %s *****' % (label, score, bars)
            else:
                print '%s %s %s' % (label, score, bars)
        else:
            std_score = '%.5f' % std[l]
            if label_best is not None and l in label_best:
                print '%s %s (%s) %s *****' % (label, score, std_score, bars)
            else:
                print '%s %s (%s) %s' % (label, score, std_score, bars)


def shorten_label(label, maxsize):
    if len(label) <= maxsize:
        return label
    else:
        keep_idx = label.index("[")
        size = maxsize - len(label[keep_idx:])
        lstart = label[:keep_idx]
        nth_letter = 0
        if len(lstart) % maxsize == 0:
            nth_letter = int(len(lstart)/size)
        else:
            nth_letter = int(len(lstart)/size)+1

        return lstart[0:len(lstart):nth_letter]+label[keep_idx:]
