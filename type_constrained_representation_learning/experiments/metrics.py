from sklearn.metrics import precision_recall_curve, roc_auc_score
from sklearn.metrics import auc


def auprc(t, p):
    precision, recall, _ = precision_recall_curve(t.flatten(), p.flatten())
    score = auc(recall, precision)
    return score


def auroc(t, p):
    score = roc_auc_score(t.flatten(), p.flatten())
    return score
