import numpy as np
import os


def tolist(x):
    return x if isinstance(x, list) else [x]


def print_experiment_details(dataset, X, splits, settings_parser, model,
                             setting):
    print 'Experiments on %s ' % dataset
    print '-' * len('Experiments on %s ' % dataset)
    print 'Tensor Shape: %d x %d x %d' % (X.shape[0], X.shape[1], X.shape[2])
    print 'Possible Entries: %d/%d => %f' % (X.size, np.prod(X.shape),
                                             float(X.size)/np.prod(X.shape))
    print 'Triples: %d' % X.nnz
    for i in xrange(X.shape[2]):
        shape0 = (len(X.type_constraints[i][0])
                  if X.type_constraints[i][0] is not None else X.shape[0])
        shape1 = (len(X.type_constraints[i][1])
                  if X.type_constraints[i][1] is not None else X.shape[1])
        pentries = shape0*shape1
        print ('Slice %d Type Constraints  %d x %d = %d entries, %d nonzeros '
               '(%.5f)'
               % (i, shape0, shape1, pentries, X[i].nnz,
                  X[i].nnz/float(pentries)))
    print
    print "Model: %s" % model
    settings_parser.show_settings(model, setting)


def delete_files(folder_path, delete_folder=False):
    for f in os.listdir(folder_path):
        os.remove(os.path.join(folder_path, f))
    if delete_folder:
        os.rmdir(folder_path)
