import argparse
from argparse import RawTextHelpFormatter
import logging
import os
import pickle
import re
import sys

from type_constrained_representation_learning.data_structures\
     .triple_tensor.validate import validate_on_triple_tensor_single
from type_constrained_representation_learning.data_structures\
     .triple_tensor.validate import validate_on_triple_tensor
from type_constrained_representation_learning.data_structures\
     .triple_tensor.validate import evaluate_experiment
from type_constrained_representation_learning.experiments.metrics import auprc
from type_constrained_representation_learning.experiments.metrics import auroc
from type_constrained_representation_learning.experiments\
     .experiment_settings_parser import ExperimentSettingsParser
from type_constrained_representation_learning.experiments.helper import (
    print_experiment_details)
from type_constrained_representation_learning.models import RandomTensorModel
from type_constrained_representation_learning.models import Rescal2
from type_constrained_representation_learning.models import (
    TranslationalEmbeddingsModel)
from type_constrained_representation_learning.models import GoogleVaultModel


# Fixed Random Number Generator Seed
SEED = 9873243
# Supported Experiments
exp_pat = re.compile('^(\w+)_experiments$')
supported_experiments = []
for f in os.listdir('./'):
    if os.path.isdir(f) and re.search(exp_pat, f):
        supported_experiments.append(re.search(exp_pat, f).group(1))


def add_random_model():
    return [RandomTensorModel(SEED)]


###############################################################################
# ARGPARSE COMMANDLINE ARGUMENTS
parser = argparse.ArgumentParser(
    description="\n".join(['Experiments on the prepared datasets.',
                           ('Parameter tuning and test on Holdout set with '
                            'Various Models.'),
                           "Examples:",
                           "---------",
                           "View available experiments for RESCAL:",
                           "\tOne Model Setting: experiments.py -m rescal2",
                           ('\tMultiple Settings: experiments.py -m rescal2 '
                            'rescal2_tc -s Test Test'),
                           '',
                           ('Show Available Model Settings (defined in '
                            './yagoc195_experiments_settings):'),
                           ('\tAll available settings and models: '
                            'experiments.py -ls'),
                           ('\tAll settings for one model: '
                            'experiments.py -m rescal2 -ls'),
                           ('\tAll settings for two models: '
                            'experiments.py -m Rescal2 '
                            'TranslationalEmbeddingsModel -ls'),
                           ('\tSpecific settings: experiments.py '
                            '-exp yagoc195 -m rescal2 '
                            'rescal2_tc -s TuningSet1 TuningSet2 -ls'),
                           ('-------------------------------------------------'
                            '----------------------')]),
    formatter_class=RawTextHelpFormatter)

parser.add_argument(
    '--experiment', '-exp',
    help="\n".join(['The datasets name, to run the experiments on.',
                    ('Supported experiments are in a folder named '
                     '{exp_name]_experiments.'),
                    'This folder must contain:',
                    '{exp_name}_experiment.ini (model settings file)',
                    'And folder ../data/{exp_name}  must contain:',
                    'data/{exp_name}_validation.cpkl (validation dataset)',
                    'data/{exp_name}_holdout.cpkl (holdout dataset)']))
parser.add_argument(
    '--model', '-m', nargs='+', help=('Specify the model module name e.g. '
                                      'Rescal2. Can be multiple  models'))
parser.add_argument(
    '--setting', '-s', nargs='+', help=('Specify the settings name e.g. '
                                        'TuningSet1. Can be multiple settings'))
parser.add_argument(
    '--list_settings', '-ls', action='store_true',
    help="\n".join([('List settings and descriptions for a given model '
                     'setting if both is given.'),
                    ('If only model is given, show all settings for that '
                     'model.'),
                    'If nothing is given, show all settings.']))
parser.add_argument(
    '--hide_params', '-hp', action='store_false',
    help='Show only the section headings when showing all settings')
parser.add_argument('--holdout', action='store_true',
                    help='Perform Experiment on the Holdout-Set')
parser.add_argument('--workers', '-w', default=1, type=int,
                    help="\n".join([('Amount of workers (Processes) used in '
                                     'the experiments (For parallelization).'),
                                    ('Each worker will train and evaluate one '
                                     'model setting (default is 1)')]))
parser.add_argument('--debug', action='store_true',
                    help='Show more output for debugging')
args = parser.parse_args()
###############################################################################

# Check experiment flag:
if args.experiment is None:
    print 'No experiment are given!'
    print 'Supported experiments are', supported_experiments
    sys.exit(1)
elif args.experiment not in supported_experiments:
    print 'Experiment %s is not supported!' % args.experiment
    print 'Supported experiments are', supported_experiments
    sys.exit(1)

# Parse setting file
settings_file = ('./%s_experiments/%s_experiment_settings.ini'
                 % (args.experiment, args.experiment))
settings_parser = ExperimentSettingsParser(settings_file)

# Set Logging and Logging Level
_log = logging.getLogger('%s_experiments' % args.experiment)
if args.debug:
    logging.basicConfig(level=logging.DEBUG)
else:
    logging.basicConfig(level=logging.INFO)

if args.list_settings:
    # Show parameters and exit
    settings_parser.show_settings(args.model, args.setting, args.hide_params)
    sys.exit(0)

elif args.model is not None and args.setting is not None:
    # Experiments
    DATASET = './%s_experiments/%s_validation_setting.pkl' % (args.experiment,
                                                              args.experiment)
    dset = 'validation_set_tensor'
    if args.holdout:
        DATASET = ('./%s_experiments/%s_holdout_setting.pkl'
                   % (args.experiment, args.experiment))
        dset = 'holdout_set_tensor'

    # Load the data
    d = pickle.load(open(DATASET))
    X = d[dset]
    splits = d['splits_indices']
    if type(splits).__name__ == 'ndarray' and splits.ndim < 3:
        splits = [splits]

    # Show some details of the experiment
    print_experiment_details(DATASET, X, splits, settings_parser, args.model,
                             args.setting)

    # Create models based on selected setting from settings file
    models = add_random_model()
    for model, setting in zip(args.model, args.setting):
        models.extend(eval(model+".model_creator")(
            settings_parser.get_settings(model, setting)))

    # Validation on splits
    metrics = [auprc, auroc]
    if args.debug or args.workers == 1:
        score_stats, scores = (
            validate_on_triple_tensor_single(X, splits, models,
                                             metrics=metrics))
    else:
        score_stats, scores = (
            validate_on_triple_tensor(X, splits, models, workers=args.workers,
                                      metrics=metrics))

    # Print the results
    evaluate_experiment(score_stats, models, metrics, label_size_limit=100)
else:
    raise IOError('--model and --setting parameters have to be set for '
                  '%s_experiments!' % args.experiment)
