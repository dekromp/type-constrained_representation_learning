import ConfigParser
import os
import re
Config = ConfigParser.ConfigParser()
Config.read('./cora_experiment_settings.ini')


class ExperimentSettingsParser(object):
    """
    A parser for the configuration files used for hyperparameter tuning
    """
    def __init__(self, settings_file):
        self.config_parser = ConfigParser.ConfigParser()
        open(settings_file, 'r')
        self.config_parser.read(settings_file)

    def get_settings(self, model_name, setting_name):
        query = model_name+" "+setting_name
        set_dic = {}
        if query in self.config_parser.sections():
            options = self.config_parser.options(query)
            for option in options:
                set_dic[option] = (
                    [self.__parse_values(s)
                     for s in self.config_parser.get(query,
                                                     option).split(",")])
                if len(set_dic[option]) == 1:
                    set_dic[option] = set_dic[option][0]
            return set_dic
        else:
            raise IOError('Section [%s %s] could not be found.'
                          % (model_name, setting_name))

    def __parse_values(self, string):
        i = re.compile("^\d+$")
        f = re.compile("^\d*[e\.]\d+$")
        true = re.compile("^True$")
        false = re.compile("^False$")
        none = re.compile("^None$")
        if re.search(i, string):
            return int(string)
        elif re.search(true, string):
            return True
        elif re.search(false, string):
            return False
        elif re.search(none, string):
            return None
        else:
            try:
                return float(string)
            except:
                return string

    def show_settings(self, models=None, settings=None, details=True):
        """Show Settings from a settings file.

        If `model` and `setting` are `None`, then all sections are shown
        elif setting is None, all sections of the model are shown
        else show the section model_setting.

        Parameters:
        -----------
        model : string
            Model name (lower case underscore version, e.g. rescal2_tc), first
            part of the section name in the config file.
        setting : string
            Setting name, second part of the section name in the config file.

        """
        # Handle Nones from the commandline inputs
        if settings is None and models is not None:
            settings = [None]*len(models)
        if settings is None and models is None:
            settings = [settings]
            models = [models]

        for model, setting in zip(models, settings):
            # No model and setting is specified => show all
            if model is None and setting is None:
                for s in self.config_parser.sections():
                    print '[%s]' % s
                    if details:
                        for o in self.config_parser.options(s):
                            print '%s : %s' % (o, self.config_parser.get(s, o))
                        print '-' * 50
            # Model is given, but setting is not specified => show all settings
            # for the model
            elif setting is None:
                found_model = False
                m = re.compile('^%s\s+' % model)
                for s in self.config_parser.sections():
                    if re.search(m, s):
                        found_model = True
                        print '[%s]' % s
                        if details:
                            for o in self.config_parser.options(s):
                                print ('%s : %s'
                                       % (o, self.config_parser.get(s, o)))
                            print '-' * 50
                if not found_model:
                    raise IOError('There are no settings for model %s.'
                                  % model)
            elif model is None:
                raise IOError('model is None, but setting is not None, '
                              'exiting!')
            else:
                found_setting = False
                for s in self.config_parser.sections():
                    if s == '%s %s' % (model, setting):
                        found_setting = True
                        print '[%s]' % s
                        if details:
                            for o in self.config_parser.options(s):
                                print ('%s : %s'
                                       % (o, self.config_parser.get(s, o)))
                if not found_setting:
                    raise IOError('Setting [%s %s] could not be found!'
                                  % (model, setting))
