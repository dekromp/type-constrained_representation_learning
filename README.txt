This folder contains the code used for the experimental results shown in the 
paper "Type-Constrained Representation Learning in Knowledge Graphs" (ISWC 2015)

Notes:
./experiments/experiments.py is a script to manage and start various experiments 
for hyperparameter tuning or evaluation on a hold out set on DBpedia-Music, 
Freebase-150k, YAGO-195k for RESCAL, TransE or the GoogleKnowledgeVault Neural 
Network.

You have to add the project folder ("type-constrained_representation_learning")
to your $PYTHONPATH.

execute
$ cd experiments
$ python experiments.py -h 
to display more details on the execution of the script.

All experiments and models are build around a central data structure 
(CooTripleTensor, ./data_structures/triple_tensor/coo_triple_tensor.py) that 
allows the efficient storage and indexing of larger triple stores represented
as very sparse 3D tensors (entities x entities x relation-types).

The implementations of the models can be found in ./models

python packages required:
-------------------------
numpy 
scipy
sklearn
joblib
theano

We compile numpy with OpenBlas:
https://github.com/xianyi/OpenBLAS


================================================================================
Please cite:
============
Denis Krompass, Stephan Baier and Volker Tresp
Type-Constrained Representation Learning in Knowledge Graphs
International Semantic Web Conference 2015

Bibtex:
@inproceedings{KrompassISWC2015,
  author    = {Denis Krompa\ss and Stephan Baier and Volker Tresp},
  title     = {Type-Constrained Representation Learning in Knowledge Graphs},
  booktitle = {Proceedings of the 13th International Semantic Web Conference (ISWC)},
  year      = {2015}
}
================================================================================
